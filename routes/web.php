<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'lang'], function () {
    Route::get('/', "GlobalController@LandingPage")->name('home');
    Route::get('Admin/login/token=FTyrXXQorIiZYL7McLItMWKTeg0iMunzW9wgULwT', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('Adminlogin', 'Auth\LoginController@login');
    Route::group(['prefix' => 'Admin',  'middleware' => 'login'], function () {
        Auth::routes();
        Route::get('/home', 'GlobalController@adminIndex');
        Route::get('/AddGames','GlobalController@addGames');
        Route::get('/DeleteGames/{id}','GlobalController@deleteGames');
        Route::get('/EditGames/{id}','GlobalController@editGames');
        Route::get('/Duplicate/{id}','GlobalController@duplicate');
        Route::get('/Game/{id}/EditQuestion/{q_id}','GlobalController@editQuestion');
        Route::post('/UpdateQuestion','GlobalController@updateQuestion');
        Route::post('/UpdateGames','GlobalController@updateGames');
        Route::post('/CreateGames','GlobalController@createGames');
        Route::post('/CreateQuestion','GlobalController@createQuestion');
        Route::post('/Question/delete','GlobalController@removeQuestion');
        Route::get('/Question/more/{id}','GlobalController@moreQuestion');
    });

    Route::post('/quiz/register', 'GlobalController@quizRegister')->name('quiz.register_form');
    Route::get('/quiz/{token}/info', 'GlobalController@quizInfo');
    Route::get('/quiz/{token}/{user}/info', 'GlobalController@registerUser');
    Route::get("/quiz/{url}/question/{token}/{user?}","GlobalController@quiz")->name('quiz.question');
    Route::get("/quiz/register/{url}/{token}","GlobalController@register")->name('quiz.register');
    Route::get('/quiz/joker','GlobalController@quizJoker');
    Route::get('/quiz/calculating','GlobalController@quizCalculating');
    Route::get('/quiz/{url}/result/{token}/{user?}','GlobalController@quizResult')->name('quiz.result');
    Route::get('/result/token={token}','GlobalController@result');
    Route::get('/quiz/confirm','GlobalController@quizConfirm')->name('quiz.confirm');
    Route::get('/{url}/confirm/{token}','GlobalController@Confirm')->name('confirm');
    Route::get('/quiz/test','GlobalController@quizTest');
    Route::post('/quiz/test','GlobalController@quizTest');
    Route::post('/quiz/timer','GlobalController@timer');
    Route::post('/quiz/second','GlobalController@second');
    Route::post('/quiz/check','GlobalController@quizCheck');
    Route::post('/quiz/check/refresh','GlobalController@quizCheckRefresh');
    Route::get('/quiz/next','GlobalController@next');
    Route::post('/set/name','GlobalController@setname');
    Route::get('/imprint',function(){
        return view('landing.imprint');
    })->name('imprint');
    Route::get('/terms',function(){
        return view('landing.terms');
    })->name('terms');
    Route::get('/data-protection',function(){
        return view('landing.data-protection');
    })->name('data-protection');
});



