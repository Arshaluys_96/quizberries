$(document).ready(function(){

	var audioCorrect = document.createElement('audio');
	var audioWrong = document.createElement('audio');

	$(document).on('click','.btn_answers',function(e){
		e.preventDefault()
        var quiz = $(this).attr('quiz')
        if (quiz) {
            $('.btn_answers').removeAttr("quiz")
            var url = $('meta[name="url"]').attr('content');
			var token = $('meta[name="token"]').attr('content');

            $.ajax({
                type: 'post',
                url: url + '/quiz/check',
                data: {_token: token, quiz},
                success: (r) => {
                    if (r.success) {
                        if (r.correct == 0) {
                            $('.form_quiz').append(`<input type="hidden" name="quiz" value="${quiz}"/>`)
                            $('.form_quiz').submit()
                        } else {
                            if (r.right == r.your) {
								$(this).css({ 'background': '#359a35','color': 'white'})
								// var fromtopsize = ($('#imagebox').height() + $('#starttitle').height() + 80) / 2;
								// var fromside = (($('#progbar').width() / 2) - 130);
								// $('#correctslide').css({
								// 	'display': 'block',
								// 	'top' : fromtopsize,
								// 	'left' : fromside
								// }).addClass("jello-horizontal");
								$('#intfact').css('display','block');
								$('#more_info').css('display','block');
								Swal.fire({
									position: 'center',
									icon: 'success',
									showConfirmButton: false,
									timer: 1500,
									width: '200px'
								});
								
								if (r.correct_audio != null) {
									audioCorrect.setAttribute('src',url+"/audio/"+r.correct_audio)
								}  else {
									audioCorrect.setAttribute('src',url+"/audio/correct.mp3")
								}
								audioCorrect.play();
                            } else {
								// var fromtopsize = ($('#imagebox').height() + $('#starttitle').height() + 80) / 2;
								// var fromside = (($('#progbar').width() / 2) - 130);
								// $(this).css({ 'background': '#c53030','color':'white'})
								// $('#wrongslide').css({
								// 	'display': 'block',
								// 	'top': fromtopsize,
								// 	'left': fromside
								// }).addClass("jello-horizontal");
								$('#intfact').css('display','block');
								$('#more_info').css('display','block');
								Swal.fire({
									position: 'center',
									icon: 'error',
									showConfirmButton: false,
									timer: 1500,
									width: '200px'
								});
								
								if (r.incorrect_audio != null) {
									audioWrong.setAttribute('src',url+"/audio/"+r.incorrect_audio)
								}  else {
									audioWrong.setAttribute('src',url+"/audio/incorrect.mp3")
								}
								audioWrong.play();
                                $(".btn_answers").each(function (i) {
                                    if (i == r.right) {
										$(this).css({ 'background': '#359a35', 'color': 'white'})
                                    }
                                })
                            }
                            setTimeout(function () {
                                $('.form_quiz').append(`<input type="hidden" name="quiz" value="${quiz}"/>`)

								$('.nap').find('div').empty().append(`<button class="btn w-100 submit-btn" style="background: #4267b2; color: white;">${r.last_question}</button>`);
								goToByScroll("qad");

								setTimeout(function () {
									$('.form_quiz').submit()
								},8000)
                            },2500) }

                    }
                }
            })
        }
	})



	$('.img_for_quiz').click(function () {
		$(this).parents('form').submit()
	})

	setTimeout(function () {
		var url = $('meta[name="url"]').attr('content');
		var token = $('meta[name="token"]').attr('content');

		$.ajax({
			type: 'post',
			url: url + '/quiz/check/refresh',
			data: {_token: token},
			success: (r) => {
				if (r.success) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text:r.answered_txt,
						footer: `<button class="btn w-100 submit-btn" style="background: #4267b2; color: white;">${r.last_question}</button>`
					});
					$('.nap').find('div').empty().append(`<button class="btn w-100 submit-btn" style="background: #4267b2; color: white;">${r.last_question}</button>`)
				}
			}
		})
	},1000)

	$(document).on('click','.submit-btn',function () {
		$('.form_quiz').submit()
	})


    $('.reg_show').hide();

    $(document).on('click','.for_regular',function(){
       $(".reg_show").show();
    })

    $(document).on('click','.iq_quiz',function(){
        $(".reg_show").hide();
    })


	$(document).on('click','.joker',function(){
		setTimeout(function(){
			$('.joker_m').removeProp("display");

		},100)
	})

	$(document).on('click','.edit_question',function(){
       var person = prompt("Please enter your question number:", 1);
		if (person != null || Number(person) > 0) {
		    window.location.href = $('#url').val()+"/Admin/Game/"+$('#id').val()+"/EditQuestion/"+person;
		}
	})

    $(document).on('click','.delete_question',function(){
         let person = prompt("Please enter your question number:", 1);
        if (person != null || Number(person) > 0) {
            let url = $('meta[name="url"]').attr('content');
            let token = $('meta[name="token"]').attr('content');
            let game_id = $('#id').val()
            $.ajax({
                type: 'post',
                url: url + '/Admin/Question/delete',
                data: {_token: token,game_id:game_id, id:person},
                success: (r) => {
                    if (r.success) {
                        alert('Success')
                    }
                }
            })
        }
    })

	$('#imghiden').change(function (e) {
		if(e.target.checked){

			$(this).val(1)
			$('.image_hide_to').parent().hide()
		}else{

			$(this).val(0)
			$('.image_hide_to').parent().show()
		}
	})

    $('#correct').change(function (e) {
		if(e.target.checked){

			$(this).val(1)
		}else{

			$(this).val(0)
		}
	})

	$('#home_screen').change(function (e) {
		if(e.target.checked){

			$(this).val(1)
		}else{

			$(this).val(0)
		}
	})

	$('#change_audio_file').change(function (e) {
		if(e.target.checked){

			$(this).val(1)
		}else{

			$(this).val(0)
		}
	})

	$('#content_type_0').click(function () {
		$('.image_hide_to').parent().show()
		$('.text_hide_to').parent().hide()
	})

	$('#content_type_1').click(function () {
		$('.image_hide_to').parent().hide()
		$('.text_hide_to').parent().hide()
	})

	$('#content_type_2').click(function () {
		$('.text_hide_to').parent().show()
		$('.image_hide_to').parent().hide()
	})


	$('#stopwatch').change(function (e) {
		if(e.target.checked){
			$(this).val(1)
			$('#timer').prop( "required", true );
		}else{
			$(this).val(0)
			$('#timer').prop( "required", false );
		}
	})

	$('#countdown').change(function (e) {
		if(e.target.checked){
			$(this).val(1)
			$('#countdown_timer').prop( "required", true );
		}else{
			$(this).val(0)
			$('#countdown_timer').prop( "required", false );
		}
	})

	function goToByScroll(id) {
		// Remove "link" from the ID
		id = id.replace("link", "");
		// Scroll
		$('html,body').animate({
			scrollTop: $("#" + id).offset().top
		}, 'slow');
	}

	

	$('.answer_number').change(function () {
		if($(this).val() == 2){

			$('input[name="answer_rigth"]').eq(2).parents('.answers').hide();
			$('input[name="answer_rigth"]').eq(3).parents('.answers').hide();
			$('input[name="answer_rigth"]').eq(2).prop( "checked", false );
			$('input[name="answer_rigth"]').eq(3).prop( "checked", false );
			$('input[name="answer3"]').prop( "required", false );
			$('input[name="answer4"]').prop( "required", false );


		}else{
			$('input[name="answer3"]').prop( "required", true );
			$('input[name="answer4"]').prop( "required", true );
			$('input[name="answer_rigth"]').eq(2).parents('.answers').show();
			$('input[name="answer_rigth"]').eq(3).parents('.answers').show();
		}
	})

	$('.btn-your-name').click(function () {
		let name = $('#yourName').val()
		if (name != ''){
			$('.yourName').find('input').remove()
			$('.yourName').find('button').remove()
			$('.yourName').find('p').text(name)

			let description = $("meta[property='og:description']").attr('content')
			description = description.replace('Friend', name)
			$("meta[property='og:description']").attr('content',description)


			let url = $('meta[name="url"]').attr('content');
			let token = $('meta[name="token"]').attr('content');
			let t = $('meta[property="og:url"]').attr('content');
			t= t.split('result/');
			$.ajax({
				type: 'post',
				url: url + '/set/name/',
				data: {_token: token,name,param:t[1]}
			})
		}
	})


})
