<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_question', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('start_id');
            $table->integer('game_id');
            $table->integer('your_answer')->nullable();
            $table->integer('right_answer')->default('0');
            $table->integer('second')->default('0');
            $table->tinyInteger('check')->default('0');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_question');
    }
}
