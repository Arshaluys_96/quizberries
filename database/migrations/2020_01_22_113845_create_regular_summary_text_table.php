<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegularSummaryTextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regular_summary_text', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id');
            $table->longtext('summary_text_20')->nullable();
            $table->longtext('summary_text_40')->nullable();
            $table->longtext('summary_text_60')->nullable();
            $table->longtext('summary_text_80')->nullable();
            $table->longtext('summary_text_100')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regular_summary_text');
    }
}
