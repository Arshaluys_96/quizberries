<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('joker')->default('0');
            $table->integer('right_answer')->default('0');
            $table->integer('game_id');
            $table->integer('question_number')->default('1');
            $table->integer('timer')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start');
    }
}
