<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('url')->nullable();
            $table->string('img')->nullable();
            $table->integer('img_hide')->default('0');
            $table->longtext('sub_title');
            $table->longtext('description');
            $table->longtext('info');
            $table->longtext('quiz_ad1');
            $table->longtext('quiz_ad2');
            $table->longtext('quiz_ad_top');
            $table->longtext('summary_text')->nullable();
            $table->tinyInteger('type')->default('0');
            $table->integer('visit')->default('0');
            $table->string('css')->nullable();
            $table->string('language')->default('en');
            $table->integer('timer')->default('0');
            $table->tinyInteger('stopwatch')->default('0');
            $table->tinyInteger('correct')->default('0');
            $table->tinyInteger('home_screen')->default('0');
            $table->string('audio_correct')->nullable();
            $table->string('audio_incorrect')->nullable();
            $table->string('box_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
