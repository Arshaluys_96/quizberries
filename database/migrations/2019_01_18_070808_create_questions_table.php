<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id');
            $table->integer('question_number');
            $table->string('title');
            $table->longtext('description');
            $table->string('image')->nullable();
            $table->integer('countdown_timer')->default('0');
            $table->integer('content_type')->default('0');
            $table->longText('html_box')->nullable();
            $table->integer('confirm')->default('0');
            $table->integer('answer_number')->default('4');
            $table->longText('question_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
