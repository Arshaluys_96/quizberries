<?php

return [
	'confirm_you_answered' => 'Has elegido',
	'confirm_click_next' => 'Haz click en "Siguiente" para proceder a la próxima pregunta.',
	'confirm_nteresting_fact' => 'Hecho interesante:',
	'confirm_btn_next' => 'Siguiente',
	'header_more_quizzes' => 'Más pruebas',
	'footer_navigation_home' => 'INICIO',
	'footer_navigation_imprint' => 'PROPIETARIO DEL SITIO',
	'footer_navigation_terms' => 'TÉRMINOS',
	'footer_navigation_data_protection' => 'PROTECCIÓN DE DATOS',
	'footer_disclaimer' => "Descargo de responsabilidad: Nuestro contenido es creado con el único propósito de divertir y entretener. Por favor no use Trendida para tomar decisiones de vida importantes y recuerde que el propósito únicamente es divertir.",
	'template_would' => '¿Te gustaría usar tu comodín para eliminar 2 respuestas incorrectas? Solo puedes usar esto una vez por cuestionario.',
	'template_action_yes' => 'Sí !!', 
	'template_action_no' => 'No',
	'main_top_quizzes' => 'TOP Quizzes',
	'main_new_quizzes' => 'Nuevos cuestionarios',
	'main_iq_test' => 'IQ Test',
	'start_share_on_facebook' => 'Compartir en Facebook',
	'start_share_on_whatsapp' => 'Compartir en WhatsApp',
	'start_ask_on_whatsapp' => 'Preguntar en WhatsApp',
	'whatsapp_message' => 'Oye 🤓 %0ANecesito tu ayuda con esta pregunta: %0A',
	'whatsapp_message2' => '👇 Haz click abajo para probarte 👇',
	'start_ob_title' => 'Dale un vistazo a este fantástico contenido patrocinado',
	'start_ob_subtitle' => 'Continúa con la prueba de coeficiente intelectual después de esto',
	'start_joker' => 'Usar Comodín 50-50',
	'share_more_quizzes' => 'MORE QUIZZES',
	'share_my_iq_is' => 'Mi coeficiente intelectual es',
	'share_i_am_clever' => '¡Soy inteligente y original!',
	'share_great_you_scored' => 'Genial, obtuviste un',
	'share_as_a_last_step' => 'Como último paso, ¡comparte tu resultado en Facebook y desafía a tus amigos!',
	'result_more_quizzes' => 'Más pruebas',
	'result_my_iq_is' => 'Mi coeficiente intelectual es',
	'result_i_am_clever' => '¡Soy inteligente y original!',
	'result_great_you_scored' => 'Genial, obtuviste un',
	'result_share_on_facebook_so_your_friends' => '¡Comparte en Facebook para que tus amigos puedan tener una oportunidad!',
	'result_share_on_facebook' => 'Compartir en Facebook',
	'quiz_start_quiz' => 'Comenzar Quiz',
	'loader_calculating_result' => 'Calculando Resultado...',
	'loader_calculating_wait' => 'Por favor espera',
	'loader_title' => '👏🎉 Bien hecho!!! 👏🎉 ',
	'loader_interesting_fact' => 'Hecho interesante:',
    'share_my_iq' => '¡Mi coeficiente intelectual es ',
    'share_cleaver' => '! ¡Soy inteligente y original!',
    'share_great' => 'Genial, obtuviste un',
    'share_description' => 'La mayoría de las personas poseen un IQ (CI) entre 85 y 115 - estás tú en el 5% que poseen más de 125?',
    'share_do_you' => ' Tienes un alto IQ (CI)?',
	'register' => 'register'
]

 ?>