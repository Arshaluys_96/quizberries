@extends('landing_inc.template')
@section('contents')
<div class="container mt-3">
	<div class="row">
		<div class="col-md-9 col-12">
		<div class='row for_padding_top'>
			<div class='col-md-12'>
				<div class='col-md-12'>
					<h2 class="mainh2">
						@lang('message.main_top_quizzes')
					</h2>
				</div>
				<div class='row '>
					@foreach( $games_top as $game_top )
						<div class='col-md-4 col-12 class_for_custom_bootstrap'>
							<a class='a_for_game_page' href='{{ URL("quiz/".$game_top->url."/info")}}'>
								<div class='full_width_div'>
									@if(!is_null($game_top->img))
									<div class='div_for_image'>
										<img style='height:170px' class='img-fluid img_class_games' src='{{ asset("images/$game_top->img") }}' alt='{!! $game_top->title !!}'>
									</div>
									@endif
									<div class='div_for_title'>
										{!! $game_top->title !!}
									</div>
								</div>
							</a>
						</div>
					@endforeach
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-12'>
				<div class='col-md-12'>
					<h2 class="mainh2">
						@lang('message.main_new_quizzes')
					</h2>
				</div>
				<div class="row">
					@foreach( $games as $game )
						<div class='col-md-4 col-12 class_for_custom_bootstrap'>
							<a class='a_for_game_page' href='{{ URL("quiz/".$game->url."/info")}}'>
								<div class='full_width_div'>
									@if(!is_null($game->img))
									<div class='div_for_image'>
										<img style='height:170px' class='img-fluid img_class_games' src='{{ asset("images/$game->img") }}'  alt='{!! $game->title !!}'>
									</div>
									@endif
									<div class='div_for_title'>
										{!! $game->title!!}
									</div>
								</div>
							</a>
						</div>
					@endforeach
				</div>
			</div>
		</div>

<!-- Native Network Start -->

<div class="mt-4">
@include('landing_inc.tabo2')
	</div>

<!-- Native network end -->

		</div> <!-- End col-md-9 col-12 -->

		<div class="col-md-3 col-12">
		@include('landing_inc.600ad')
		</div>
		
	</div> <!-- End row -->

	<div class='row pt-2 pr-3'>
		<div class='col-md-9 third_part_bottom'>
			<div class='row'>
				<div class='col-md-4 col-11 text-md-center'>
					<i class="fas fa-chevron-right"></i>
					<a href='#' class='a_for_bottom_content_menu'>
						<span class='bottom_content_menu_part'> @lang('message.main_top_quizzes') </span>
					</a>
				</div>
			</div>
		</div>
	</div>	
</div>

@endsection