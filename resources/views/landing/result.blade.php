<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-KH3STTR');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Trendida - Trends the fun way!</title>
	@if(!empty($game->css))
		<link rel="stylesheet" href="{{ asset('css/landing/'.$game->css) }}">
	@else
		<link rel="stylesheet" href="{{ asset('css/landing/style.css') }}">
	@endif
	<link rel="stylesheet" href="{{ asset('css/landing/custom.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Work+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<meta name="url" content="{{ URL('/') }}"/>
	<meta name="token" content="{{ csrf_token() }}"/>
	@if($start->registerForm)
		<meta property="wa:url" content="{{ URL("quiz/".$game->url."/".$start->registerForm->unique_id."/info") }}"/>
	@else
		<meta property="wa:url" content="{{ URL("quiz/".$game->url."/info") }}"/>
	@endif
	<meta property="fb:app_id"      content="514162265872345" />
	<meta property="og:site_name" 	content="trendida.com" />
	<meta property="og:title" 		content="{{$game->title}}" />
	<meta property="og:type" 		content="website">
	@if($start->registerForm)
		<meta property="og:url" content='{{ URL("/quiz/".$game->url."/result/".$token."/".$start->registerForm->unique_id) }}'/>
	@else
 		<meta property="og:url" content='{{ URL("/quiz/".$game->url."/result/".$token."/") }}'/>
	@endif
	@if($game->result_image)
		<meta property="og:image" content='{{ "https://trendida.com/images/".$game->result_image }}'/>
	@else
		<meta property="og:image" content='https://trendida.com/images/trophy.jpg'/>
	@endif
	<!-- <meta property="og:image" content='{{ asset("images/$game->img") }}'/> -->
	<meta property="og:description" content="{{!empty($start->name)?$start->name:'Friend'}} {{ $game->box_info }}"/>
	<meta property="og:image:width" content="800">
	<meta property="og:image:height" content="620">
</head>
<body>
<nav id="navbarnew" class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="container">
		<a class="navbar-brand  li_for_logo_img" href="{{URL('/')}}"><img src="{{ asset('images/logo.png') }}"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item li_for_header_item">
					<a class="nav-link a_class_for_item_menu" href="https://trendida.com"><span class='class_for_hover_effect_menu'>@lang('message.result_more_quizzes') </span></a>
				</li>
			</ul>
		</div>
	</div>
</nav>

<div class="container">
	<div class="game-custom">
		<div class='row '>

			<div class="col-12 mb-2">
					@include('landing_inc.finaltop')
			</div>

			<div class='col-md-12'>
				<div class="container">
				<!-- Score text -->
					@if($game->type == \App\Games::iq_quiz)
						<h3 class="text-center h3top">@lang('message.result_my_iq_is') {{number_format($result)}}! @lang('message.result_i_am_clever')</h3>
					@else
						<h3 class="text-center h3top">@lang('message.result_you_got') {{count($correct_answers)}} @lang('message.result_out_of') {{$question}} @lang('message.result_questions')!</h3>
					@endif
					
					<div class="row justify-content-center ">
						<!-- <button class="btn ui newgame button"><i class="text-white"></i> משחק חדש </button> -->
					<!-- Summary text -->
						<h4  class="m-1 w-100 text-center">{!! $summary_text !!} </h4>
			
						@if(!$start->registerForm && in_array($game->register_form,[3,4]))
							<div class="w-100">
							<form action="{{route('quiz.register_form')}}" method="post">
								@csrf
								<input type="hidden" name="start_id" value="{{base64_encode($start->id)}}">
								<div class="form-register">
									@if($game->register_form_id == 1)
									<div class="form-group">
										<label for="name">Name</label>
										<input type="text" id="name" name="name" class="form-control">
									</div>
									@endif
									<div class="form-group">
										<label for="phone">Phone</label>
										<input type="text" id="phone" name="phone" class="form-control">
									</div>
									<div class="form-group text-center">
										<button class="btn btn-info mx-auto">@lang('message.register')</button>
									</div>
								</div>
							</form>
							</div>
						@endif

						<h4 class="text-center">@lang('message.create_trophy')</h4>	
						<div class="yourName w-100 text-center">
							@if(empty($start->name))
							<input class="trophyinput" type="text" maxlength="15" id="yourName" placeholder="@lang('message.trophy_namehere')"><button class="btn-your-name trophybtn">@lang('message.trophy_send')</button>
							@endif
							<label for="yourName">
							<div class="trophy" style="{{$game->result_image?'background-image: url(/images/'.$game->result_image.')':'' }} ">
								<div class="trophytext">
									<p class="text-center mt-1 mb-0 trophyfont">
										@if(!empty($start->name)) {{$start->name}} @else @lang('message.trophy_name') @endif
									</p>
								</div>
								</div>
							</label>
						</div>

</div>
						</div>

						<!-- Share buttons + Image -->
						<!-- <div class="row justify-content-between col-md-12"> -->
						<div class="col-md-12">

							<h5  class="text-center w-100"> @lang('message.result_share_on_facebook_so_your_friends')</h5>
							<button class="btn ui facebookfinal button"><i class="fab fa-facebook-square text-white"></i> @lang('message.result_share_on_facebook')</button>
							<button class="btn ui whatsappfinal button"><i class="fab fa-whatsapp-square text-white"></i> @lang('message.start_share_on_whatsapp') </button>

							<!-- Game Image - Share on click -->
							<!-- <img class="result_img" src='{{ asset("images/$game->img") }}' alt=""> -->

						</div>

						<!-- Correct answers of the user -->
							<div class="paperholder">
								<div class="papertop">
									<h4 class="mt-4 w-100 text-center paperttitle">@lang('message.you_answered')</h4>
								</div>
								<div class="papermid">
									<div class="col-md-12 papertext">
										@foreach($correct_answers as $answer)
											<div><span>Q{{$answer['question_number']}}: </span><span style="font-size: 20px; color: blue;">{{$answer['question']}}</span> - <span>{{$answer['answer']}}</span></div>
										@endforeach
									</div>
								</div>
								<div class="paperbot"></div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-12">
						<div class="d-flex justify-content-center">
						@include('landing_inc.728ad')
						</div>

</div>



<iframe src="{{ asset("/audio/finish.mp3") }}" type="audio/mp3" allow="autoplay" class="d-none"></iframe>
<!-- <iframe src="https://d1490khl9dq1ow.cloudfront.net/audio/sfx/mp3preview/BsTwCwBHBjzwub4i4/crowd-cheer-clap-scream_fJkt-OV__NWM.mp3" type="audio/mp3" allow="autoplay" class="d-none"></iframe> -->

<div class='container'>
	<div class='row pt-5 pr-3 footerpad'>
		<div class='col-md-12 col-12 footer_part'>
			<ul class='footer_ul pt-3'>
				<li class='footer_ul_li' ><a href="{{ route('home') }}" class='a_footer_menu'> @lang('message.footer_navigation_home') </a></li>
				<li class='footer_ul_li' ><a href="{{ route('imprint') }}" class='a_footer_menu'> @lang('message.footer_navigation_imprint') </a></li>
				<li class='footer_ul_li' ><a href="{{ route('terms') }}" class='a_footer_menu'> @lang('message.footer_navigation_terms') </a></li>
				<li class='footer_ul_li' ><a href="{{ route('data-protection') }}" class='a_footer_menu'> @lang('message.footer_navigation_data_protection') </a></li>
			</ul>
		</div>
		<div class='col-md-12 col-12'>
			<div class='bottom_desc'>
				@lang('message.footer_disclaimer')
			</div>
			<div class='bottom_desc'>
				© 2019 TRENDIDA
			</div>
		</div>
	</div>
</div>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '514162265872345',
            xfbml      : true,
            version    : 'v3.2'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(".ui.facebookfinal.button").click(function() {
        FB.ui({
            method: 'share',
            href: $("meta[property='og:url']").attr('content')
        }, function(response){});
    })
</script>

<script>
    $(".ui.whatsappfinal.button").click(function() {
		var url = $("meta[property='wa:url']").attr('content');
		var title = $("meta[property='og:title']").attr('content');
		var description = $("meta[property='og:description']").attr('content');
		var image = $("meta[property='og:image']").attr('content');
        // window.open('https://wa.me/?text='+ encodeURIComponent(image) + '%0A'+title + '%0A' + description + '%0A' + url,'width=400,height=300,resizable=yes');
		window.open('https://api.whatsapp.com/send?text=' + description + '%0A %0A' + url,'width=400,height=300,resizable=yes');

	})
	
	// $(".ui.newgame.button").click(function() {
	// 	window.open({{ URL("quiz/".$game->url."/info") }},'width=400,height=300,resizable=yes');
	// })

</script>



<script src="{{ asset('js/landing/script.js') }}"></script>
<script src="{{ asset('js/landing/confetti.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script>
    jQuery(document).ready(function($){
        setInterval(function(){
            $('#remove-me').remove();
        },3000);
    });
</script>
<script>
	jQuery(document).ready(function($){
    setInterval(function(){
        $('#remove-me').remove();
    },15000);
});
</script>
</body>
</html>
