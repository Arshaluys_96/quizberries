@extends('landing_inc.template')
@section('contents')
<script src="https://trendida.com/js/landing/sweetalert2.all.min.js"></script>
<link rel="stylesheet" href="https://trendida.com/js/landing/sweetalert2.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.7.1/dist/sweetalert2.min.css"></script>


	<div class="container mt-3">
	<div class='row'>
		<div class='col-md-9 col-12'>
			<form action="{{ URL('quiz/test') }}" method="post" class="form_quiz">
				@csrf

				@if (count($game->questions) == 0)
        			<script>window.location.href = "{{URL('/') }}" </script>
        			<?php die; ?>
				@endif

				<!-- <div class="row justify-content-between facebookpad hideit">
                         <button class="btn ui facebook button"><i class="fab fa-facebook-square text-white"></i> @lang('message.start_share_on_facebook') </button>
						 <button class="btn ui whatsapp button"><i class="fab fa-whatsapp-square text-white"></i> @lang('message.start_ask_on_whatsapp') </button>
                 </div> -->
				<div class="sliderholder">
					<div id="correctslide">@lang('message.correctbtn')</div>
					<div id="wrongslide">@lang('message.wrongbtn')</div>
				</div>

				@if(Session::get('count_question'))
		  		    <h4 id="starttitle" class="mt-2 mb-2 p-2 bg-secondary text-white quiz_start_title">{!! $game->questions[Session::get('count_question')-1]->title !!}</h4>
                    <?php
	                    $min = floor($starts->timer/60);
	                    $sec = $starts->timer%60 === 0?'00':$starts->timer%60;
                    ?>
					@if($game->stopwatch == 1)
						<h5 class="mt-0 mb-2 p-4 bg-primary text-white ">Countdown: <span class="timer">{{ $min.':'.$sec  }} </span></h5>
					@endif

				<div id="qad" class="col-12 mt-4 mb-4">
					@include('landing_inc.respad')
				</div>

					@if($game->questions[Session::get('count_question')-1]->content_type == 0)

						<div class=" w-100 start_image">
							<div class="d-flex justify-content-center">
								<img  style='object-fit: cover;' src='{{ asset("images/".$game->questions[Session::get('count_question')-1]->image) }}'>
							</div>
						</div>
					@elseif($game->questions[Session::get('count_question')-1]->content_type == 2)
							<div  class=" w-100 start_image">
								<div class="d-flex justify-content-center">
									{!! $game->questions[Session::get('count_question')-1]->html_box !!}
								</div>
							</div>
					@endif

				@else


					<h4 id="starttitle" class="mt-0 mb-2 p-4 bg-secondary text-white quiz_start_title">{!! $game->questions[$starts->question_number-1]->title !!}</h4>
					@if($game->stopwatch == 1)
                        <?php
	                        $min = floor($starts->timer/60);
	                        $sec = $starts->timer%60 === 0?'00':$starts->timer%60;
                        ?>
					<h5 class="mt-0 mb-2 p-4 bg-primary text-white ">
						Countdown: <span class="timer"> {{ $min.':'.$sec  }}</span>
					</h5>
					@endif
					<div id="progbar" class="progress">
						<div class="progress-bar" style="width:{{round(Session::get('counts')*100/count($game->questions)).'%'}}"></div>
						<span class="progress_span">
							{!! round(Session::get('counts')*100/count($game->questions)) !!}%
						</span>
					</div>


					@if($game->questions[$starts->question_number-1]->content_type == 0)
					<div id="imagebox" class=" w-100 start_image">
						<div class="d-flex justify-content-center">
							<img  style='object-fit: fill;' src='{{ asset("images/".$game->questions[$starts->question_number-1]->image) }}'>
						</div>
					</div>
					@elseif($game->questions[$starts->question_number-1]->content_type == 2)
						<div id="imagebox" class=" w-100 start_image">
							<div class="d-flex justify-content-center">
								{!! $game->questions[$starts->question_number-1]->html_box !!}
							</div>
						</div>
					@endif

				@endif



				@if(count($starts->start_questions))
					@if($starts->start_questions[$starts->start_questions->count()-1]->second > 0)
					<div class="w-100 text-center p-3 text-white" style="background: #b33838">
						<h4 id="countdown_sec">{{$starts->start_questions[$starts->start_questions->count()-1]->second}}</h4>
					</div>
					@endif
				@endif



			</form>
		  		@if(Session::get('Joker'))
					<div class="row justify-content-center">
						@foreach(Session::get('Joker') as $answer)
						<div class="col-md-6 col-12 mt-2">
							<button type="button" class="btn w-100 btn_answers"  style="display: none" quiz="{{base64_encode($answer['id'])}}">{!! $answer['answer'] !!}</button>
						</div>
						@endforeach
					</div>
				<?php Session::forget('Joker'); ?>
				@else
					<div class="row justify-content-center">
						@if(Session::get('count_question'))
							@foreach($game->questions[Session::get('count_question')-1]->answers as $key => $answer)
							<div class="col-md-6 col-12  mt-2">
								<button type="button" class="btn w-100 btn_answers"  style="display: none" quiz="{{base64_encode($key)}}">{!! $answer->answer !!}</button>
							</div>
							@endforeach
						@else
							@foreach($game->questions[$starts->question_number-1]->answers as $key => $answer)
								<div class="col-md-6 col-12  mt-2">
									<button type="button" class="btn w-100 btn_answers"  style="display: none" quiz="{{base64_encode($key)}}">{!! $answer->answer!!}</button>
								</div>
							@endforeach
						@endif
					</div>
				@endif

				<div id="qad" class="col-12 mt-4 mb-4">
					@include('landing_inc.respad')
				</div>
            <div class="row justify-content-center nap">
                <div class="col-md-12 col-12  mt-2">
                </div>
            </div>

				<!-- <div class="justify-content-center facebookpad mt-4 lifelines">

					<h2>@lang('message.need_help')</h2>
						<p>@lang('message.need_help2')</p>
				</div> -->

				<!-- Native Network Start -->

				<!-- Native network end -->

				
				<div id="intfact" class="mt-4 interesting">
					<div class="md-2 fact-img">

					</div>
        			<h4>@lang('message.confirm_nteresting_fact')</h4>
					@if(Session::get('count_question'))
			            <div>{!! $game->questions[Session::get('count_question')-1]->description!!}</div>
					@else
			            <div>{!! $game->questions[$starts->question_number-1]->description!!}</div>
					@endif
			    </div>


				<div class="row justify-content-between facebookpad mt-2">
                         <button class="btn ui facebookfinal button"><i class="fab fa-facebook-square text-white"></i> @lang('message.start_share_on_facebook') </button>
						 <button class="btn ui whatsappfinal button"><i class="fab fa-whatsapp-square text-white"></i> @lang('message.start_ask_on_whatsapp') </button>
                 </div>



				<!-- @if($starts->joker == 0)
					<div class="row justify-content-center">
					    <button data-toggle="modal"  class="btn joker" data-target="#myJoker">
							<i class="far fa-lightbulb"></i> @lang('message.start_joker')</button>
					</div>
				@endif -->


			@if(Session::get('count_question') && !empty($game->questions[Session::get('count_question')-1]->question_info))

				<div id="more_info" class="mt-4 more_info">

					<h4>@lang('message.more_info')</h4>
					<div>{!! $game->questions[Session::get('count_question')-1]->question_info!!}</div>
				</div>
			@elseif(!empty($game->questions[$starts->question_number-1]->question_info))
				<div id="more_info" class="mt-4 more_info">
					<h4>@lang('message.more_info')</h4>
						<div>{!! $game->questions[$starts->question_number-1]->question_info!!}</div>
				</div>
			@endif




		</div>

		@if ($agent->isDesktop())
		<div class='col-md-3 col-12'>
			@include('landing_inc.600ad')
		</div>
		@endif



	</div>
</div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '514162265872345',
            xfbml      : true,
            version    : 'v3.2'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(".ui.facebookfinal.button").click(function() {
        FB.ui({
            method: 'share',
            href: $("meta[property='og:url']").attr('content')
        }, function(response){});
    })
</script>

<script>
    $(".ui.whatsappfinal.button").click(function() {
		var url = $("meta[property='og:url']").attr('content');
        window.open("https://api.whatsapp.com/send?text=@lang('message.whatsapp_message')" + "*{!! $game->questions[$starts->question_number-1]->title !!}*" + "%0A %0A" +  "@lang('message.whatsapp_message2')" + url ,"width=400,height=300,resizable=yes");
    })

    var i = 0
    setInterval(function(){
        $('.btn_answers').eq(i).show('slow','swing')
	    i++
    }, 600);
</script>

<script>
	$(document).ready(function () {
        if ($('#countdown_sec').length > 0) {
            var interval = setInterval(function () {
                var url = $('meta[name="url"]').attr('content');
                var token = $('meta[name="token"]').attr('content');
                $.ajax({
                    type:'post',
                    url: url+'/quiz/second',
                    data:{_token:token},
                    success: function(r){
                        if (r.success) {
                            var time = r.second
                            $('#countdown_sec').text(time)
                        } else {
                            $('#countdown_sec').text('Time is up!')
                            $('.btn_answers').prop('disabled','disabled')
                            $(".btn_answers").each(function (i) {
                                if (i == r.right) {
                                    $(this).css({ 'background': '#359a35', 'color': 'white'})
                                }
                            })
	                        if (r.last_question) {

                                $('.nap').find('div').empty().append(`<a href="${url}/quiz/next" class="btn w-100" style="background: #4267b2; color: white;">${ "@lang('message.finish_question')" }</a>`)
	                        } else {

                                $('.nap').find('div').empty().append(`<a href="${url}/quiz/next" class="btn w-100" style="background: #4267b2; color: white;">${ "@lang('message.next_question')" }</a>`)
                            }
                            clearInterval(interval)
                        }
                    }
                })
            },1000)
        }
    })
</script>

@if($game->stopwatch == 1)
	<script>
		setInterval(function(){
		var url = $('meta[name="url"]').attr('content');
		var token = $('meta[name="token"]').attr('content');
		$.ajax({
		type:'post',
		url: url+'/quiz/timer',
		data:{_token:token},
		success: function(r){
			if (r.success){
			    var time = r.time
                var min = Math.floor((time % 3600) / 60);
                var sec = time % 60;
                var sec_min = "";
                sec_min += "" + min + ":" + (sec < 10 ? "0" : "");
                sec_min += "" + sec;
                $('.timer').text(sec_min)
			}else{
                location.reload();
			}
		}
		})
		}, 1000);
	</script>

@endif

@endsection
