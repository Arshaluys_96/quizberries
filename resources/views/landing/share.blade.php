<!doctype html>
<html  lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Trendida - Trends the fun way!</title>
	@if(!empty($game->css))
		<link rel="stylesheet" href="{{ asset('css/landing/'.$game->css) }}">
	@else
		<link rel="stylesheet" href="{{ asset('css/landing/style.css') }}">
	@endif
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

	<meta property="og:type" 		content="website">
	@if(!empty($start))
		@if($game->type == \App\Games::iq_quiz)
			<meta property="og:title" 		content="@lang('message.share_my_iq') {{number_format($result)}} @lang('message.share_cleaver')" />
		@else
			<meta property="og:title" 		content="@lang('message.share_great') {{number_format($result)}}%!" />
		@endif
		<meta property="og:url" 		content="{{ URL('/result/token='.base64_encode($start->id.' '.$game->title)) }}" />
	@else
		<meta property="og:title" 		content="@lang('message.share_do_you')" />
		<meta property="og:url" 		content="{{ URL('/result/token='.base64_encode('results '.$game->id)) }}" />
	@endif
	<meta property="og:image" 		content='{{ asset("images/$game->img") }}' />
	<meta property="og:description" content="@lang('message.share_description')" />
	<meta property="og:site_name" 	content="trendida.com" />
	<meta property="fb:app_id"      content="514162265872345" />
	<meta property="og:image:width" content="800">
	<meta property="og:image:height" content="420">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="container">
		<a class="navbar-brand  li_for_logo_img" href="{{URL('/')}}"><img src="{{ asset('images/logo.png') }}" alt=""></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item li_for_header_item">
					<a class="nav-link a_class_for_item_menu" href="https://trendida.com"><span class='class_for_hover_effect_menu'>@lang('message.share_more_quizzes')</span></a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<div class="container">
	<div class='row '>
		<div class='col-md-8 offset-2'>
			<div class="container">
				<div class="row justify-content-center ">
					@if(!empty($result))
						@if($game->type == \App\Games::iq_quiz)
							<h3 class="text-center"> @lang('message.share_my_iq_is') {{number_format($result)}}! @lang('message.share_i_am_clever')</h3>
						@else
							<h3 class="text-center"> @lang('message.share_great_you_scored') {{number_format($result)}}%!</h3>
						@endif
					@endif
					<div class="row justify-content-center w-75 mt-2">
						<h4 style="font-size: 12px;">@lang('message.share_as_a_last_step') </h4>
						<img class="share" src='{{ asset("images/$game->img") }}' alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class='container'>
	<div class='row pt-5 pr-3 footerpad'>
		<div class='col-md-12 footer_part'>
			<ul class='footer_ul pt-3'>
				<li class='footer_ul_li' ><a href='{{ route('home') }}' class='a_footer_menu'>@lang('message.footer_navigation_home')</a></li>
				<li class='footer_ul_li' ><a href='{{ route('imprint') }}' class='a_footer_menu'>@lang('message.footer_navigation_imprint')</a></li>
				<li class='footer_ul_li' ><a href='{{ route('terms') }}' class='a_footer_menu'>@lang('message.footer_navigation_terms')</a></li>
				<li class='footer_ul_li' ><a href='{{ route('data-protection') }}' class='a_footer_menu'>@lang('message.footer_navigation_data_protection')</a></li>
			</ul>
		</div>
		<div class='col-md-12'>
			<div class='bottom_desc'>
				@lang('message.footer_disclaimer')
			</div>
			<div class='bottom_desc'>
				© 2019 Trendida
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('js/landing/script.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>
