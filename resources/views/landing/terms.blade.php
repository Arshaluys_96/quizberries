@extends('landing_inc.template')
@section('contents')
<div class="container mt-3">
	<div class='row'>
		<div class='col-md-8 col-12 '>
		    <h2>Terms</h2>

		    <h4>1 Object of terms of participation and use</h4>
		    <ul class="terms">
		        <li>1.1 ViralApps (hereinafter referred to as “VA”) is the operator of Trendida.com</li>
		        <li>1.2 VA provides to you on the web pages of Trendida (hereinafter “VA Web Pages”) and in various apps (hereinafter “VA Apps”) different free services (e.g. quiz-games) (hereinafter “VA Games”) for entertainment purposes.</li>
		        <li>1.3 The present terms of participation and use govern the provision of VA Games on VA Web Pages and VA Apps as well as your use of those and all content provided. They also govern your participation in sweepstakes organized by VA (hereinafter “Sweepstakes”).</li>
		        <li>1.4 Any additional rules and conditions of participation will be published as part of the particular VA Game. By participating, you accept these rules and conditions of participation.</li>
		    </ul>
		    <h4>2 Change of terms of participation and use</h4>
		    <ul class="terms">
		        <li>2.1 VA is entitled to modify these terms of participation and use at any time within its contractual relationship with you.</li>
		        <li>2.2 You can view the terms of participation and use at any time at https://trendida.com/terms. We will also inform you by email of any changes to our terms of participation and use as long as you have registered with VA.</li>
		        <li>2.3 By continuing to use VA Games after a change, you are accepting the new terms of participation and use.</li>
		    </ul>
		    <h4>3 Registration</h4>
		    <ul class="terms">
		        <li>3.1 Use of VA Games is generally free of charge and does not require your registration.</li>
		        <li>3.2 Depending on the internet connection used and your internet rate, you may, however, incur internet connection charges which will be charged to you by third parties (e.g. internet provider, mobile network provider).</li>
		        <li>3.3 Certain additional features on VA’s Web Pages and VA Apps (e.g. rankings, highscore rankings, competitions) as well as participation in Sweepstakes (§ 5 of the terms of participation and use) require you to register as a participant. There is no entitlement of participation. VA is therefore entitled to reject applications to participate without further explanation.</li>
		        <li>3.4 In the course of the registration process, you will be asked to enter a user name and password. You can then use these data to log in to the VA website after activation. You must keep your access data including your password secret and you must not allow unauthorized third parties to access them. You are liable in accordance with statutory provisions for any use and/or other activity carried out using your access data.</li>
		        <li>3.5 VA Games are designed for children and adolescents. However, VA recommends use of VA Games from the age of 8. If you are younger than 8, we only recommend use of VA Games under parental supervision.</li>
		    </ul>
		    <h4>4 Availability, Modifications</h4>
		    <ul class="terms">
		        <li>4.1 VA always endeavors to keep VA Games complete, up-to-date and fully functional on VA Web Pages and VA Apps, but temporary restrictions or interruptions cannot be ruled out as a result of technical malfunctions (e.g. interruptions to the power supply, hardware or software errors, and technical problems in data lines).</li>
		        <li>4.2 Otherwise the right to use VA Games available on VA Web Pages is limited to VA’s technical and operating capabilities.</li>
		        <li>4.3 VA reserves the right to shut down, restrict or change the operation of VA’s Web Pages and/or VA Games wholly or partially, at any time, without explanation.</li>
		    </ul>
		    <h4>5 Rights of use</h4>
		    <ul class="terms">
		        <li>5.1 All copyrights and rights of use to VA Games and/or VA Apps as well as to the content on VA Web Pages (text, graphics, etc.) are expressly reserved.</li>
		        <li>5.2 Unless any further use is explicitly permitted in these terms of participation and use or on VA Web Pages,</li>
		        <li>a) you may only retrieve and display the content available on VA Web Pages for personal purposes. This right of use is limited to the duration of your contractual participation;</li>
		        <li>b) you may not process, change, translate, present, publish, exhibit, duplicate or distribute in any other way the content available on VA Web Pages either in whole or in part. You are also forbidden from removing or altering copyright notices, logos or other markings or proprietary notices.</li>
		        <li>5.3 Any forbidden exploitation, distribution, duplication or other breach of commercial property rights and copyrights belonging to VA will be pursued in the civil and criminal courts.</li>
		    </ul>
		    <h4>6 Forbidden activities</h4>
		    <ul class="terms">
		        <li>6.1 The games available on VA web pages or through VA Apps are only intended for your non-commercial use. You are prohibited from any use for or in connection with commercial purposes. Prohibited commercial use includes in particular</li>
		        <li>– any offer or advertising of paid content, services and/or products, both your own and those belonging to third parties,</li>
		        <li>– any offer, advertising or execution of activities with a commercial background such as sweepstakes, draws, barters, adverts or pyramid schemes, and</li>
		        <li>– any electronic or other gathering of identity data and/or contact details (including email addresses) belonging to members (e.g. for the dispatch of unsolicited emails).</li>
		        <li>6.2 You are also prohibited from any activities on or in connection with VA Web Pages, VA Games or VA Apps which are in breach of current legislation, infringe the rights of third parties or breach the principles of protection for minors.</li>
		        <li>6.3 You are also prohibited from engaging in any action liable to impair the smooth operation of VA web pages, VA Games and VA Apps, in particular to overload VA’s systems.</li>
		        <li>6.4 If there is a suspicion of illegal or criminal acts, VA will be entitled and possibly also obliged to check your activities and if necessary to initiate appropriate legal steps.</li>
		    </ul>
		    <h4>7 Data protection</h4>
		    <ul class="terms">
		        <li>7.1 VA will process your personal details for specific purposes and in accordance with statutory provisions.</li>
		        <li>7.2 Personal details given for the purpose of using additional features in VA Games or for participating in Sweepstakes, will be used by VA for fulfilment and handling.</li>
		        <li>7.3 Further information on the nature, scope, location and purpose of VA’s collection, processing and use of required personal details can be found in the data protection declaration https://trendida.com/data-protection.</li>
		    </ul>
		    <h4>9 Limitation of Liability</h4>
		    <ul class="terms">
		        <li>9.1 If you suffer a loss from using the free services provided on VA Web Pages or through VA Apps (including the retrieval of free content), VA will only be liable to the extent that your loss has been incurred due to the contractual use of the free content and/or services and only in the case of deliberate acts (including fraudulent intent) and gross negligence on the part of VA.</li>
		        <li>9.2 VA is not liable for incorrect information caused and/or disseminated by participants and/or third parties including Cooperation Partners, and which is connected to a competition.</li>
		        <li>9.3 VA is not liable for offers from third parties, in particular from Cooperation Partners, which are promoted on VA Web Pages, in VA Games or VA Apps.</li>
		    </ul>
		    <h4>10 Final provisions</h4>
		    <ul class="terms">
		        <li>10.1 Changes to these terms of participation and use must be made in writing to be effective. The same applies to the abrogation of the requirement for written form.</li>
		        <li>10.2 If any provisions of these terms of participation and use are or become invalid and/or unenforceable, the validity of the remaining provisions will not be affected.</li>
		        <li>10.3 The place of fulfilment and jurisdiction is, to the extend permitted by law, Tel-Aviv, Israel</li>
		    </ul>
		</div>
	</div>	
</div>

@endsection