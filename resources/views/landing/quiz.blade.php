<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KH3STTR');</script>
	<!-- End Google Tag Manager -->

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Trendida - Trends the fun way!</title>
	@if(url()->current() == URL('/'))
		<link rel="stylesheet" href="{{ asset('css/landing/style.css') }}">
	@elseif(!empty($game->css))
		<link rel="stylesheet" href="{{ asset('css/landing/'.$game->css) }}">
	@elseif(!empty($quiz->css))
		<link rel="stylesheet" href="{{ asset('css/landing/'.$quiz->css) }}">
	@else
		<link rel="stylesheet" href="{{ asset('css/landing/style.css') }}">
	@endif
	<link rel="stylesheet" href="{{ asset('css/landing/custom.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Work+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<meta name="url" content="{{ URL('/') }}"/>
	<meta name="token" content="{{ csrf_token() }}"/>
	<link rel="apple-touch-icon" sizes="57x57" href="/images/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/images/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/images/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/images/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/images/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/images/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/images/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/images/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/images/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
	<link rel="manifest" href="/images/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/images/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
		<meta property="fb:app_id"      content="514162265872345" />
		<meta property="og:site_name" 	content="trendida.com" />
		<meta property="og:title" 		content="{{$quiz->title}}" />
		<meta property="og:type" 		content="website">
		<meta property="og:url" content='{{ URL("quiz/".$quiz->url."/info")}}'/>
		<meta property="og:image" content='https://trendida.com/images/trophy.jpg'/>
		@if(Session::get('userName'))
			<meta property="og:description" content="{{Session::get('userName')}} {{ $quiz->box_info }}"/>
            <?php Session::forget('userName'); ?>
		@else
			<meta property="og:description" content="Friend {{ $quiz->box_info }}"/>
		@endif
		<meta property="og:image:width" content="800">
		<meta property="og:image:height" content="620">

<!-- Google Auto Ads -->
	<script data-ad-client="ca-pub-4044693535716961" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KH3STTR"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="myJoker" class="modal fade joker_m" role="dialog" >
	<!-- Modal content-->
	<div class="modal-dialog">
		<div class="modal-content joker_modal">
			<div class="row justify-content-center">
				<i class="far fa-lightbulb fa-3x" ></i>
			</div>
			<div class="mt-4 text-center">
				<p>@lang('message.template_would')</p>
			</div>
			<div class="actions">
				<div class="row justify-content-center">
					<a href="{{ URL('/quiz/joker') }}" class="btn btn-outline-success">
						<i class="fa fa-check"></i> @lang('message.template_action_yes')
					</a>
					<button class="ml-2 btn btn-outline-danger" type="button"  data-dismiss="modal">
						<i class="fas fa-times"></i> @lang('message.template_action_no')
					</button>
				</div>
			</div>
		</div>
	</div>
</div>

@include('landing_inc.header')


	<div class="container">

				@if(!empty($quiz->quiz_ad_top))
					<div class="">
						{!!  $quiz->quiz_ad_top !!}
					</div>
				@endif

		<div class='row pt-3'>
			<div class='col-md-12 col-12 start_quiz'>
				<div class='div_for_title_game text-center'>
					<h1>
						{!! $quiz->title !!}
					</h1>
				</div>


					<p class='pt-0'>
						{!! $quiz->sub_title !!}
					</p>
				<form action="{{ route('quiz.register',['url' => $quiz->url,'token' => $encode])}}" method="get">

				@if($quiz->img_hide == 0)
				<center>
					<a class='a_for_start_quiz mt-2'>
						<img class='img-fluid img_for_quiz' src='{{ asset("images/$quiz->img") }}'> 
					</a>
				</center>
				@endif

				@if(!empty($quiz->quiz_ad1))
					<div class="col-12 quiztopad mt-2">
						{!!  $quiz->quiz_ad1 !!}
					</div>
				@endif
			
				<!-- <div class="col-12">
				@include('landing_inc.infeedad')
				</div> -->

					@if(in_array($quiz->register_form,[2,4]))
						<div class=" form-register">
							@if($quiz->register_form_id == 1)
							<div class="form-group d-block">
								<label for="name">Name</label>
								<input type="text" id="name" name="name" class="form-control">
							</div>
							@endif
							<div class="form-group d-block">
								<label for="phone">Phone</label>
								<input type="text" id="phone" name="phone" class="form-control">
							</div>
						</div>
					@endif
						<a class='a_for_start_quiz mt-2' >
							<center>
								<button class='btn btn-white btn_for_start_quiz' title='{{$quiz->title}}'>@lang('message.quiz_start_quiz')</button>
							</center>
						</a>
				</form>

				<p class='pt-3'>
					{!! $quiz->info !!}
				</p>


				<!-- Native Network Start -->


				@if(!empty($quiz->quiz_ad2))
					<div class="col-12 zeropad">
						{!!  $quiz->quiz_ad2 !!}
					</div>
				@endif


				<!-- Native network end -->




			</div>
		</div>
	</div>

@include('landing_inc.footer')
