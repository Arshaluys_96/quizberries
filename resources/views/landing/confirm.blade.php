@extends('landing_inc.template')
@section('contents')
	<div class="container">
		<div class="game-custom">
			<div class='row'>
				<div class='col-md-12'>
					@if($next)
                        <?php $quiz = Session::get('quiz'); ?>
						<h4 class="mt-2 mb-2 p-2 text-center"> @lang('message.confirm_you_answered') "{!!  $game->questions[$count]->answers[$quiz]->answer !!}". @lang('message.confirm_click_next')</h4>
					@endif
                    <div class="col-md-12 interesting">
						<div class="md-2 fact-img">		
						</div>
					<h4> @lang('message.confirm_nteresting_fact') </h4>
					<div>{!!  $game->questions[$count]->description !!} </div>
					</div>
				</div>

				<!-- Native Network Start -->

				<div class="col-md-12 mt-3">
				@include('landing_inc.infeedad')
				</div>

				<!-- Native network end -->


			</div>
		</div>

		<div class="mt-4">
		<div class="ad-label ad-visible">-advertisement-</div>
				@include('landing_inc.respad')
		</div>

		

		@if($next)
		<div class="row justify-content-center">
			<a href="{{ URL('/quiz/confirm') }}" class="btn confirmed"> @lang('message.confirm_btn_next') </a>
			
		</div>
		
		@endif
	</div>

@endsection