@extends('landing_inc.template')
@section('contents')
<div class="container">
	<div class='row pt-3'>
		<div class='col-md-12'>

				<h1 class="text-center">
					@lang('message.loader_title')
				</h1>
				
			<div class="row justify-content-center">
				<!-- <img src='{{ asset("images/loader.gif") }}'> -->
				
				<div class="loader">
					<div class="loader__bar"></div>
					<div class="loader__bar"></div>
					<div class="loader__bar"></div>
					<div class="loader__bar"></div>
					<div class="loader__bar"></div>
					<div class="loader__ball"></div>
				</div>
				
			</div>
			<p class="calctext text-center animate">@lang('message.loader_calculating_result')</p>
			<p class="calctext text-center">@lang('message.loader_calculating_wait')</p>
			
			<!-- <div class="col-md-12 mt-3">
			@include('landing_inc.infeedad')
			</div> -->

			<!-- <div class="mt-4 ">
				<h4>@lang('message.loader_interesting_fact')</h4>
				<div>{!!  $question->description !!}</div>
			</div> -->

		</div>
	</div>	
</div>
<script>

  myVar = setTimeout(result, 2000);

function result() {
	window.location.href = "{{ Route('quiz.result',['url' => $game->url,'token' => $token])}}";
}
</script>
@endsection