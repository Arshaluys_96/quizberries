@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{ URL('Admin/CreateQuestion') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12 mt-2">
                        <label for="title">Question Number</label>
                        <input type="text" id="question_number"  disabled value="{{ $q_number }}" class="form-control" >
                    </div>
                    <div class="col-md-12 mt-2">
                        <label for="title">Title</label>
                        @if ($errors->has('title'))
                            <p class="text-danger">{{ $errors->first('title') }}</p>
                        @endif
                        <input type="text" name="title" id="title" required="required" class="form-control">
                    </div>
                    <input type="hidden" name="question_number" value="{{ $q_number }}" required="">
                    <div class="col-md-12 mt-2">
                        <label for="title">Interesting Fact</label>
                        @if ($errors->has('interesting_fact'))
                            <p class="text-danger">{{ $errors->first('interesting_fact') }}</p>
                        @endif
                        <textarea name="interesting_fact" id="interesting_fact" required="required" class="form-control"></textarea>
                    </div>

                    <div class="col-md-12 mt-2">
                        <label for="title">Question info (bottom of page)</label>
                        @if ($errors->has('question_info'))
                            <p class="text-danger">{{ $errors->first('question_info') }}</p>
                        @endif
                        <textarea name="question_info" id="question_info"  class="form-control"></textarea>
                    </div>


                    <div class="col-md-12 mt-2">
                        <label for="countdown">Countdown Timer?</label>
                        <input type="checkbox" id="countdown" name="countdown" value="0">

                        @if ($errors->has('countdown_timer'))
                            <p class="text-danger">{{ $errors->first('countdown_timer') }}</p>
                        @endif
                        <br>
                        <input type="text" name="countdown_timer" id="countdown_timer"  class="form-control">
                        <span>Seconds</span>
                    </div>

                    <div class="col-md-12 mt-2">
                        <label >Content Type</label>
                        <br>
                        <input type="radio" name="content_type" id="content_type_0" value="0" checked> Image
                        <input type="radio" name="content_type" id="content_type_1" value="1"> No Image
                        <input type="radio" name="content_type" id="content_type_2" value="2"> HTML Box
                    </div>

                    <div class="col-md-12 mt-2">
                        <label for="image">Image</label>
                        @if ($errors->has('image'))
                            <p class="text-danger">{{ $errors->first('image') }}</p>
                        @endif
                        <input type="file" name="image" id="image"  class="form-control p-1 image_hide_to image_show_file">
                        <img id="image_preview" src="" alt="your image" style="margin:5px; max-width:350px; max-height: 200px; object-fit: cover;display: none" />

                    </div>

                    <div class="col-md-12 mt-2"  style="<?php echo $errors->has('html_box')?'display:block':'display:none'  ?>">
                        <label for="image">HTML Box</label>
                        @if ($errors->has('html_box'))
                            <p class="text-danger">{{ $errors->first('html_box') }}</p>
                        @endif
                        <textarea name="html_box" id="html_box"  class="form-control p-1 text_hide_to"></textarea>
                    </div>


                    <div class="col-md-12 mt-2">
                        <label>Answer Number</label>
                        <br>
                        <input type="radio" name="answer_number" value="4"  class="answer_number" checked> 4 or
                        <input type="radio" name="answer_number" value="2" class="answer_number" > 2 or
                        <input type="radio" name="answer_number" value="0" class="answer_number" > None
                    </div>

                     <div class="answers col-md-12 mt-2">
                        <div class="row">
                            <div class="col-md-1">
                                <label >Right?</label>
                                <input type="radio" required="" name="answer_rigth" value="1">
                            </div>
                            <div class="col-md-11">
                                <label for="title">Answer 1</label>
                                @if ($errors->has('answer'))
                                    <p class="text-danger">{{ $errors->first('answer1') }}</p>
                                @endif
                                <input type="text" name="answer1"  required="required" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="answers col-md-12 mt-2">
                        <div class="row">
                            <div class="col-md-1">
                                <label >Right?</label>
                                <input type="radio" required="" name="answer_rigth" value="2">
                            </div>
                            <div class="col-md-11">
                                <label for="title">Answer 2</label>
                                @if ($errors->has('answer'))
                                    <p class="text-danger">{{ $errors->first('answer2') }}</p>
                                @endif
                                <input type="text" name="answer2"  required="required" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="answers col-md-12 mt-2">
                        <div class="row">
                            <div class="col-md-1">
                                <label >Right?</label>
                                <input type="radio" required="" name="answer_rigth" value="3">
                            </div>
                            <div class="col-md-11">
                                <label for="title">Answer 3</label>
                                @if ($errors->has('answer'))
                                    <p class="text-danger">{{ $errors->first('answer3') }}</p>
                                @endif
                                <input type="text" name="answer3"  required="required" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="answers col-md-12 mt-2">
                        <div class="row">
                            <div class="col-md-1">
                                <label >Right?</label>
                                <input type="radio" required="" name="answer_rigth" value="4">
                            </div>
                            <div class="col-md-11">
                                <label for="title">Answer 4</label>
                                @if ($errors->has('answer'))
                                    <p class="text-danger">{{ $errors->first('answer4') }}</p>
                                @endif
                                <input type="text" name="answer4"  required="required" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="answers col-md-12 mt-2">
                        <div class="row">
                            <div class="col-md-1">
                                <input type="radio"  name="confirm" value="1">
                            </div>
                            <div class="col-md-11">
                               <p>Confirmed</p>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="{{ URL('Admin/home') }}" class="btn btn-danger float-right m-2">Finish</a>
                <button class="btn btn-success mt-2 float-right">Next</button>
            </form>
        </div>
    </div>
</div>
@endsection
