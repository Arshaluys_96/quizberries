@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{ URL('Admin/UpdateQuestion') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" id="id" value="{{ $question->id }}">
                <div class="row">
                <!-- The question number -->
                    <div class="col-md-12 mt-2">
                        <label for="title">Question Number</label>
                        <input type="text" id="question_number"  disabled value="{{ $question->question_number }}" class="form-control" >
                    </div>

                    <div class="col-md-12 mt-4">
                    <h2>Question Content</h2>
                    </div>

                    <!-- Question Title -->
                    <div class="col-md-12 mt-2">
                        <label for="title">Title</label>
                        @if ($errors->has('title'))
                            <p class="text-danger">{{ $errors->first('title') }}</p>
                        @endif
                        <input type="text" name="title" id="title" required="required" class="form-control" value="{{ $question->title }}">
                    </div>

                    <!-- Set up content type -->
                    <div class="col-md-6 mt-2">
                        <label >Content Type</label>
                        <br>
                        <input type="radio" name="content_type" id="content_type_0" value="0" {{ $question->content_type == 0?'checked="checked"':'' }}> Image
                        <input type="radio" name="content_type" id="content_type_1" value="1" {{ $question->content_type == 1?'checked="checked"':'' }}> No Image
                        <input type="radio" name="content_type" id="content_type_2" value="2" {{ $question->content_type == 2?'checked="checked"':'' }}> HTML Box
                    </div>

                    <!-- Game Image -->
                    <div class="col-md-6 mt-2" style="<?php echo $question->content_type == 0?'display:block':'display:none'  ?>">
                        <label for="image">Image</label>
                        @if ($errors->has('image'))
                            <p class="text-danger">{{ $errors->first('image') }}</p>
                        @endif

                        <input type="file" name="image" id="image"   class="form-control p-1 image_hide_to image_show_file">
                        @if($question->content_type == 0)
                            <img id="image_preview" src="{{ asset("images/$question->image") }}" alt="your image" style="margin:5px; max-width:350px; max-height: 200px; object-fit: cover;" />
                        @else
                            <img id="image_preview" src="" alt="your image" style="margin:5px; max-width:350px; max-height: 200px; object-fit: cover;display: none" />
                        @endif
                    </div>

                    <!-- HTML BOX -->
                    <div class="col-md-6 mt-2"  style="<?php echo $errors->has('html_box') || $question->content_type == 2?'display:block':'display:none'  ?>">
                        <label for="image">HTML Box</label>
                        @if ($errors->has('html_box'))
                            <p class="text-danger">{{ $errors->first('html_box') }}</p>
                        @endif
                        <textarea name="html_box" id="html_box"  class="form-control p-1 text_hide_to">{{ $question->html_box }}</textarea>
                    </div>



                    <!-- Answers -->
                    <div class="col-md-12 mt-2">
                        <label>How many answers?</label>
                        <br>
                        <input type="radio" name="answer_number" value="4"  class="answer_number" <?php echo $question->answer_number == 4?'checked="checked"':''  ?>> 4 or <input type="radio" name="answer_number" <?php echo $question->answer_number == 2?'checked="checked"':''  ?> value="2" class="answer_number" > 2
                    </div>
                    @foreach($question->answers as $key => $answer)
                    <div class="answers col-md-12 mt-2">
                        <div class="row">
                            <div class="col-md-1">
                                <label >Right?</label>
                                <input type="radio" required="" <?php echo $answer->right == 1?'checked="checked"':''  ?> name="answer_rigth" value="{{$key+1}}">
                            </div>
                            <div class="col-md-11">
                                <label for="title">Answer {{$key+1}}</label>
                                @if ($errors->has('answer'))
                                    <p class="text-danger">{{ $errors->first('answer4') }}</p>
                                @endif
                                <input type="text" name="answer{{ $key+1 }}" required="required"  class="form-control" value="{{ $answer->answer }}" >
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @if($question->answer_number == 2)
                        <div class="answers col-md-12 mt-2" style="display:none;">
                            <div class="row">
                                <div class="col-md-1">
                                    <label >Right?</label>
                                    <input type="radio" required="" name="answer_rigth" value="3">
                                </div>
                                <div class="col-md-11">
                                    <label for="title">Answer 3</label>
                                    @if ($errors->has('answer'))
                                        <p class="text-danger">{{ $errors->first('answer3') }}</p>
                                    @endif
                                    <input type="text" name="answer3" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="answers col-md-12 mt-2" style="display:none;">
                            <div class="row">
                                <div class="col-md-1">
                                    <label >Right?</label>
                                    <input type="radio" required="" name="answer_rigth" value="4">
                                </div>
                                <div class="col-md-11">
                                    <label for="title">Answer 4</label>
                                    @if ($errors->has('answer'))
                                        <p class="text-danger">{{ $errors->first('answer4') }}</p>
                                    @endif
                                    <input type="text" name="answer4"  class="form-control">
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="col-md-12 mt-4">
                    <h2>Question Settings</h2>
                    </div>

                    <!-- Show middle page -->
                    <div class="col-md-6 mt-2">
                        <div class="row">
                            <div class="col-md-1">
                                <input type="radio" <?php echo $question->confirm == 1?'checked="checked"':''  ?> name="confirm"  value="1">
                            </div>
                            <div class="col-md-11">
                               <p>Confirm page (Middle page)</p>
                            </div>
                        </div>
                    </div>
                    <!-- Dont show middle page -->
                    <div class="col-md-6 mt-2">
                        <div class="row">
                            <div class="col-md-1">
                                <input type="radio" <?php echo $question->confirm == 0?'checked="checked"':''  ?> name="confirm"  value="0">
                            </div>
                            <div class="col-md-11">
                               <p>No Confirm page</p>
                            </div>
                        </div>
                    </div>

                    <!-- Fact that will be shown after question -->
                    <div class="col-md-12 mt-2">
                        <label for="title">Interesting Fact (Shown on middle page)</label>
                        @if ($errors->has('interesting_fact'))
                            <p class="text-danger">{{ $errors->first('interesting_fact') }}</p>
                        @endif
                        <textarea name="interesting_fact" id="interesting_fact" required="required" class="form-control">{{ $question->description }}</textarea>
                    </div>

                    <div class="col-md-12 mt-2">
                        <label for="title">Question info (bottom of page)</label>
                        @if ($errors->has('question_info'))
                            <p class="text-danger">{{ $errors->first('question_info') }}</p>
                        @endif
                        <textarea name="question_info" id="question_info"  class="form-control">{{ $question->question_info }}</textarea>
                    </div>

                    <!-- Create countdown for this question? -->
                    <div class="col-md-10">
                        @if ($errors->has('countdown_timer'))
                            <p class="text-danger">{{ $errors->first('countdown_timer') }}</p>
                        @endif
                        <br>

                        <input type="text" name="countdown_timer" id="countdown_timer"  class="form-control" value="{{$question->countdown_timer}}">
                        <span>Seconds</span>
                    </div>

                    <div class="col-md-12  mt-3">
                        <button  class="btn btn-success ml-2 float-right">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
