@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{ URL('Admin/CreateGames') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">

                    <div class="col-md-12 mt-2">
                    <h2>Game Setup</h2>
                    </div>
                <!-- Pick Language -->
                    <div class="col-md-3 mt-2">
                        <label for="language">Language</label>
                        @if ($errors->has('language'))
                            <p class="text-danger">{{ $errors->first('language') }}</p>
                        @endif
                        <select name="language" required="" id="language"  class="form-control">
                            <option value="" selected="" disabled="">Select language</option>
                            <option value="en">English</option>
                            <option value="es">Spanish</option>
                            <option value="he">Hebrew</option>
                        </select>
                    </div>

                    <!-- Pick style sheet -->
                    <div class="col-md-3 mt-2">
                        <label for="language">Style</label>
                        @if ($errors->has('style'))
                            <p class="text-danger">{{ $errors->first('style') }}</p>
                        @endif
                        <select name="css" required="" id="language"  class="form-control">
                            <option value="" selected="" disabled="">Select CSS File</option>
                            <option selected="selected">style.css</option>
                            <option>he-style.css</option>
                            <!--<option>style2.css</option>
                            <option>style3.css</option> -->
                        </select>
                    </div>

                    <!-- Set game type -->
                    <div class="col-md-6 mt-2">
                        <label for="quiz_type">Game Type</label>
                        <div class="row" id="quiz_type">
                            <div class="col-md-3 ">
                                <div class="checkbox">
                                    <label><input class="iq_quiz" type="radio"  name="quiz_type" value="0"  required>IQ quiz</label>
                                </div>
                            </div>
                            <div class="col-md-3 " >
                                <div class="checkbox" >
                                    <label><input type="radio" class="for_regular"  name="quiz_type" value="1" checked required>Regular quiz</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Set up custom URL for game -->
                    <div class="col-md-12 mt-2">
                        <label for="title">URL (No Spaces, small letters, can use - seperator)</label>
                        @if ($errors->has('url'))
                            <p class="text-danger">{{ $errors->first('url') }}</p>
                        @endif
                        <input type="text" name="url" id="url" required="required" class="form-control" placeholder="example: iqtest-v1">
                    </div>

                    <div class="col-md-12 mt-4">
                    <h2>Game Info</h2>
                    </div>

        <hr class="mb-4">

                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="home_screen" name="home_screen" value="0">
                  <label class="custom-control-label" for="home_screen">Show Game on homescreen?</label>
                </div>

                    <!-- set home screen -->
                    <!-- <div class="col-md-12 mt-2">
                        <label for="home_screen">Show Game on homescreen?</label>
                        <input type="checkbox" id="home_screen" name="home_screen" value="0">
                    </div> -->

                    <div class="col-md-12 mt-2">
                        <label for="change_audio_file">Change Audio Files?</label>
                        <input type="checkbox" id="change_audio_file" name="change_audio_file" value="0">
                    </div>

                    <div class="col-md-6 mt-2">
                        <label for="correct_audio">Correct Audio</label>
                          @if ($errors->has('correct_audio'))
                            <p class="text-danger">{{ $errors->first('correct_audio') }}</p>
                        @endif
                        <input type="file" id="correct_audio" name="correct_audio" >
                    </div>

                    <div class="col-md-6 mt-2">
                        <label for="incorrect_audio">Incorrect Audio</label>
                        @if ($errors->has('incorrect_audio'))
                            <p class="text-danger">{{ $errors->first('incorrect_audio') }}</p>
                        @endif
                        <input type="file" id="incorrect_audio" name="incorrect_audio" >
                    </div>

                    <!-- Set up game title -->
                    <div class="col-md-12 mt-2">
                        <label for="title">Title (Capital Letter only, Make it attractive)</label>
                        @if ($errors->has('title'))
                            <p class="text-danger">{{ $errors->first('title') }}</p>
                        @endif
                        <input type="text" name="title" id="title" required="required" class="form-control" placeholder="example: Ultimate Brain Quiz 2020">
                    </div>

                    <!-- Select if image is hidden or not -->
                    <div class="col-md-1 mt-2">
                        <label for="imghiden">Image hidden</label>
                        <input type="checkbox" id="imghiden" name="imghidden" value="0">
                    </div>

                    <!-- Show image and upload image -->
                    <div class="col-md-11 mt-2">
                        <label for="image">Game Image</label>
                        @if ($errors->has('image'))
                            <p class="text-danger">{{ $errors->first('image') }}</p>
                        @endif
                        <input type="file" name="image" id="image" required class="form-control p-1 ">
                    </div>

                    <!-- Subtitle -->
                    <div class="col-md-6 mt-2">
                        <label for="sub_title">Sub Title</label>
                        @if ($errors->has('sub_title'))
                            <p class="text-danger">{{ $errors->first('sub_title') }}</p>
                        @endif
                        <textarea  name="sub_title" id="sub_title" required="required" class="form-control summernote"></textarea>
                    </div>

                    <!-- Description -->
                    <div class="col-md-6 mt-2">
                        <label for="description">Description</label>
                        @if ($errors->has('description'))
                            <p class="text-danger">{{ $errors->first('description') }}</p>
                        @endif
                        <textarea name="description" id="description" required="required" class="form-control summernote"></textarea>
                    </div>

                    <!-- Info -->
                    <div class="col-md-6 mt-2">
                        <label for="info">Info</label>
                        @if ($errors->has('info'))
                            <p class="text-danger">{{ $errors->first('info') }}</p>
                        @endif
                        <textarea name="info" id="info" required="required" class="form-control summernote"></textarea>
                    </div>

                    <!-- Summary text -->
                    <div class="col-md-6 mt-2">
                        <label for="summary_text">Summary Text</label>
                        <textarea name="summary_text" id="summary_text"  class="form-control summernote"></textarea>
                    </div>

                    <!-- Box info from share -->
                    <div class="col-md-12 mt-2">
                        <label for="box_info">Box info</label>
                        <textarea name="box_info" id="box_info"  class="form-control "></textarea>
                    </div>


                    <div class="col-md-12 mt-4">
                    <h2>Advertisements</h2>
                    </div>

                    <!-- Set up top ads script -->
                    <div class="col-md-4 mt-2">
                        <label for="quiz_ad_top">Top Ads Box</label>
                        @if ($errors->has('quiz_ad_top'))
                            <p class="text-danger">{{ $errors->first('quiz_ad_top') }}</p>
                        @endif
                        <textarea name="quiz_ad_top" id="quiz_ad_top" class="form-control"></textarea>
                    </div>

                    <!-- Set up ad box 1 script -->
                    <div class="col-md-4 mt-2">
                        <label for="quiz_ad1">Ads Box 1</label>
                        @if ($errors->has('quiz_ad1'))
                            <p class="text-danger">{{ $errors->first('quiz_ad1') }}</p>
                        @endif
                        <textarea  name="quiz_ad1" id="quiz_ad1" class="form-control"></textarea>
                    </div>

                    <!-- Set up ad box 2 script -->
                    <div class="col-md-4 mt-2">
                        <label for="quiz_ad2">Ads Box 2</label>
                        @if ($errors->has('quiz_ad2'))
                            <p class="text-danger">{{ $errors->first('quiz_ad2') }}</p>
                        @endif
                        <textarea  name="quiz_ad2" id="quiz_ad2" class="form-control"></textarea>
                    </div>

                    <div class="col-md-12 mt-4">
                    <h2>Additional Settings</h2>
                    </div>

                    <!-- Choose if to stopwatch for this game -->
                    <div class="col-md-2 mt-2">
                        <label for="stopwatch">Add Stopwatch ?</label>
                        <input type="checkbox" id="stopwatch" name="stopwatch" value="0">
                    </div>

                    <!-- Set the time for stopwatch -->
                    <div class="col-md-8 mt-2">
                        <label for="timer">Timer</label>
                        @if ($errors->has('timer'))
                            <p class="text-danger">{{ $errors->first('timer') }}</p>
                        @endif
                        <input type="text" name="timer" id="timer"  class="form-control">
                    </div>

                    <!-- Show the correct/incorrect answers -->
                    <div class="col-md-2 mt-2">
                        <label for="correct">Show correct/incorrect</label>
                        <input type="checkbox" id="correct" name="correct" value="1">
                    </div>

                <!-- Summary by levels -->
                <div class="row reg_show">
                    <div class="col-md-12 mt-2">
                        <label for="quiz_ad_top">Summary Text 0%-20%</label>
                        <textarea name="summary_text_20" class="form-control"></textarea>
                    </div>

                    <div class="col-md-12 mt-2">
                        <label for="quiz_ad_top">Summary Text 20%-40%</label>
                        <textarea name="summary_text_40" class="form-control"></textarea>
                    </div>

                    <div class="col-md-12 mt-2">
                        <label for="quiz_ad_top">Summary Text 40%-60%</label>
                        <textarea name="summary_text_60" class="form-control"></textarea>
                    </div>

                    <div class="col-md-12 mt-2">
                        <label for="quiz_ad_top">Summary Text 60%-80%</label>
                        <textarea name="summary_text_80" class="form-control"></textarea>
                    </div>

                    <div class="col-md-12 mt-2">
                        <label for="quiz_ad_top">Summary Text 80%-100%</label>
                        <textarea name="summary_text_100" class="form-control"></textarea>
                    </div>

                </div>

                    <!-- Show image and upload image -->
                    <div class="col-md-11 mt-2">
                        <label for="result_image">Result Image</label>
                        @if ($errors->has('result_image'))
                            <p class="text-danger">{{ $errors->first('result_image') }}</p>
                        @endif
                        <input type="file" name="result_image" id="result_image"  class="form-control p-1 ">
                    </div>


                    <div class="col-md-12 mt-2">
                        <label for="register_form">Register Form</label>
                        <div class="row" id="register_form">
                            <div class="col-md-2 ">
                                <div class="checkbox">
                                    <label><input type="radio"  name="register_form" value="1"  required> Don’t show</label>
                                </div>
                            </div>
                            <div class="col-md-3 " >
                                <div class="checkbox" >
                                    <label><input type="radio"  name="register_form" value="2"  required> Show on game start</label>
                                </div>
                            </div>
                            <div class="col-md-3 " >
                                <div class="checkbox" >
                                    <label><input type="radio"  name="register_form" value="3"  required> Show on game result</label>
                                </div>
                            </div>
                            <div class="col-md-4 " >
                                <div class="checkbox" >
                                    <label><input type="radio"  name="register_form" value="4"  required> Show on game start and result</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="form_register_name" name="form_register_name" value="1">
                      <label class="custom-control-label" for="form_register_name">Form Register Name </label>
                    </div>
                    <div class="col-md-12 mt-3">
                        <button class="btn btn-success  float-right">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

