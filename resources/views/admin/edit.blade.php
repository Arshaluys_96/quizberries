@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{ URL('Admin/UpdateGames') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" id="id" value="{{ $game->id }}">
                <div class="row">

                    <div class="col-md-12 mt-2">
                    <h2>Game Setup</h2>
                    </div>
                <!-- Set Style -->
                    <div class="col-md-6 mt-2">
                        <label for="language">Style</label>
                        @if ($errors->has('style'))
                            <p class="text-danger">{{ $errors->first('style') }}</p>
                        @endif
                        <select name="css" required="" id="language"  class="form-control">
                            <option value="" selected="" disabled="">Select CSS File</option>
                            <option {{ $game->css == 'style.css'?'selected':'' }}>style.css</option>
                            <option {{ $game->css == 'he-style.css'?'selected':'' }}>he-style.css</option>
                            <!--<option {{ $game->css == 'style2.css'?'selected':'' }}>style2.css</option>
                            <option {{ $game->css == 'style3.css'?'selected':'' }}>style3.css</option> -->
                        </select>
                    </div>

                    <!-- Set game URL -->
                    <div class="col-md-6 mt-2">
                        <label for="title">URL</label>
                        @if ($errors->has('url'))
                            <p class="text-danger">{{ $errors->first('url') }}</p>
                        @endif
                        <input type="text" name="url" id="url" required="required" class="form-control" value="{{ $game->url }}">
                    </div>

                    <!-- Set Game Title -->
                    <div class="col-md-12 mt-2">
                        <label for="title">Title</label>
                        @if ($errors->has('title'))
                            <p class="text-danger">{{ $errors->first('title') }}</p>
                        @endif
                        <input type="text" name="title" id="title" required="required" class="form-control" value="{{ $game->title }}">
                    </div>

                    <div class="col-md-12 mt-4">
                    <h2>Game Info</h2>
                    </div>

                    <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="home_screen" name="home_screen" <?php echo $game->home_screen == 1?'checked="checked" value="1"':'value="0"'  ?>>
                    <label class="custom-control-label" for="home_screen">Show Game on homescreen?</label>
                    </div>

                    <!-- set home screen -->
                    <!-- <div class="col-md-12 mt-2">
                        <label for="home_screen">Show on homescreen?</label>
                        <input type="checkbox" id="home_screen" name="home_screen"  <?php echo $game->home_screen == 1?'checked="checked" value="1"':'value="0"'  ?>>
                    </div> -->

                    <!-- set audio file-->
                    <div class="col-md-12 mt-2">
                        <label for="change_audio_file">Change Audio Files?</label>
                        <input type="checkbox" id="change_audio_file" name="change_audio_file" value="0">
                    </div>

                    <div class="col-md-6 mt-2">
                        <label for="correct_audio">Correct Audio</label>
                        @if ($errors->has('correct_audio'))
                            <p class="text-danger">{{ $errors->first('correct_audio') }}</p>
                        @endif
                        <input type="file" id="correct_audio" name="correct_audio" >
                        @if(!empty($game->audio_correct))
                            <audio src="{{ asset("/audio/$game->audio_correct") }}" controls></audio>
                        @endif
                    </div>

                    <div class="col-md-6 mt-2">
                        <label for="incorrect_audio">Incorrect Audio</label>
                        @if ($errors->has('incorrect_audio'))
                            <p class="text-danger">{{ $errors->first('incorrect_audio') }}</p>
                        @endif
                        <input type="file" id="incorrect_audio" name="incorrect_audio" >
                        @if(!empty($game->audio_incorrect))
                            <audio src="{{ asset("/audio/$game->audio_incorrect") }}" controls></audio>
                        @endif
                    </div>

                    <!-- Set if image is hidden or not -->
                    <div class="col-md-12 mt-2">
                        <label for="imghiden">Image hidden</label>
                        <input type="checkbox" id="imghiden" name="imghidden" <?php echo $game->img_hide == 1?'checked="checked" value="1"':'value="0"'  ?>>
                    </div>

                    <!-- Preview image/Upload new image -->
                    <div class="col-md-12 mt-2">
                        <label for="image">Image</label>
                        @if ($errors->has('image'))
                            <p class="text-danger">{{ $errors->first('image') }}</p>
                        @endif
                        <img style='height: 200px;object-fit: contain;' class='img-fluid img_class_games mt-2 mb-2' src='{{ asset("images/$game->img") }}'>
                        <input type="file" name="image"  id="image" class="form-control p-1" >
                    </div>

                    <!-- Subtitle -->
                    <div class="col-md-12 mt-2">
                        <label for="sub_title">Sub Title (Text below the title)</label>
                        @if ($errors->has('sub_title'))
                            <p class="text-danger">{{ $errors->first('sub_title') }}</p>
                        @endif
                        <textarea  name="sub_title" id="sub_title" required="required" class="form-control summernote">{{ $game->sub_title }}</textarea>
                    </div>

                    <!-- Description -->
                    <div class="col-md-6 mt-2">
                        <label for="description">Description</label>
                        @if ($errors->has('description'))
                            <p class="text-danger">{{ $errors->first('description') }}</p>
                        @endif
                        <textarea name="description" id="description" required="required" class="form-control summernote">{{ $game->description }}</textarea>
                    </div>

                    <!-- Info -->
                    <div class="col-md-6 mt-2">
                        <label for="info">Info (Text below the Start button)</label>
                        @if ($errors->has('info'))
                            <p class="text-danger">{{ $errors->first('info') }}</p>
                        @endif
                        <textarea name="info" id="info" required="required" class="form-control summernote">{{ $game->info }}</textarea>
                    </div>

                    <!-- Summary text -->
                    <div class="col-md-6 mt-2">
                        <label for="summary_text">Summary Text (Text on Final Page)</label>
                        <textarea name="summary_text" id="summary_text"  class="form-control summernote">{{ $game->summary_text }}</textarea>
                    </div>

                    <div class="col-md-12 mt-2">
                        <label for="box_info">Box info (Text for Shares on WhatsApp/Facebook)</label>
                        <textarea name="box_info" id="box_info"  class="form-control ">{{ $game->box_info }}</textarea>
                    </div>

                    <div class="col-md-12 mt-4">
                    <h2>Advertisements</h2>
                    </div>

                    <!-- Set Ad top box -->
                    <div class="col-md-4 mt-2">
                        <label for="quiz_ad_top">Top Ads Box</label>
                        @if ($errors->has('quiz_ad_top'))
                            <p class="text-danger">{{ $errors->first('quiz_ad_top') }}</p>
                        @endif
                        <textarea name="quiz_ad_top" id="quiz_ad_top" class="form-control">{{ $game->quiz_ad_top }}</textarea>
                    </div>

                    <!-- Set ad Box 1 -->
                    <div class="col-md-4 mt-2">
                        <label for="quiz_ad1">Ads Box1</label>
                        @if ($errors->has('quiz_ad1'))
                            <p class="text-danger">{{ $errors->first('quiz_ad1') }}</p>
                        @endif
                        <textarea  name="quiz_ad1" id="quiz_ad1" class="form-control">{{ $game->quiz_ad1 }}</textarea>
                    </div>

                    <!-- Set ad box 2 -->
                    <div class="col-md-4 mt-2">
                        <label for="quiz_ad2">Ads Box2</label>
                        @if ($errors->has('quiz_ad2'))
                            <p class="text-danger">{{ $errors->first('quiz_ad2') }}</p>
                        @endif
                        <textarea  name="quiz_ad2" id="quiz_ad2" class="form-control">{{ $game->quiz_ad2 }}</textarea>
                    </div>

                    <div class="col-md-12 mt-4">
                    <h2>Additional Settings</h2>
                    </div>

                    <!-- Set if game has stopwatch -->
                    <div class="col-md-2 mt-2">
                        <label for="stopwatch">Stopwatch ?</label>
                        <input type="checkbox" id="stopwatch" name="stopwatch"  <?php echo $game->stopwatch == 1?'checked="checked" value="1"':'value="0"'  ?>>
                    </div>

                    <!-- Set time on stopwatch -->
                    <div class="col-md-4 mt-2">
                        <label for="timer">Timer</label>
                        @if ($errors->has('timer'))
                            <p class="text-danger">{{ $errors->first('timer') }}</p>
                        @endif
                        <?php
                            $min = floor($game->timer/60);
                            $sec = $game->timer%60 === 0?'00':$game->timer%60;
                        ?>
                        <input type="text" name="timer" id="timer"  class="form-control" value="{{ $min.':'.$sec  }}" <?php echo $game->stopwatch == 1?'required="required"':''  ?>>
                    </div>

                    <!-- Set if answers show correct/incorrect answer -->
                    <div class="col-md-6 mt-2">
                        <label for="correct">correct/incorrect</label>
                        <input type="checkbox" id="correct" name="correct" <?php echo $game->correct == 1?'checked="checked" value="1"':'value="0"'  ?>>
                    </div>


                @if($game->type == 1)
                    <!-- Summary by levels -->
                        <div class="row">
                            <div class="col-md-12 mt-2">
                                <label for="quiz_ad_top">Summary Text 0%-20%</label>
                                <textarea name="summary_text_20" class="form-control">{{$game->regularSummaryText?$game->regularSummaryText->summary_text_20:''}}</textarea>
                            </div>

                            <div class="col-md-12 mt-2">
                                <label for="quiz_ad_top">Summary Text 20%-40%</label>
                                <textarea name="summary_text_40" class="form-control">{{$game->regularSummaryText?$game->regularSummaryText->summary_text_40:''}}</textarea>
                            </div>

                            <div class="col-md-12 mt-2">
                                <label for="quiz_ad_top">Summary Text 40%-60%</label>
                                <textarea name="summary_text_60" class="form-control">{{$game->regularSummaryText?$game->regularSummaryText->summary_text_60:''}}</textarea>
                            </div>

                            <div class="col-md-12 mt-2">
                                <label for="quiz_ad_top">Summary Text 60%-80%</label>
                                <textarea name="summary_text_80" class="form-control">{{$game->regularSummaryText?$game->regularSummaryText->summary_text_80:''}}</textarea>
                            </div>

                            <div class="col-md-12 mt-2">
                                <label for="quiz_ad_top">Summary Text 80%-100%</label>
                                <textarea name="summary_text_100" class="form-control">{{$game->regularSummaryText?$game->regularSummaryText->summary_text_100:''}}</textarea>
                            </div>

                        </div>
                    @endif

                <!-- Preview Result image/Upload new image -->
                    <div class="col-md-12 mt-2">
                        <label for="result-image">Result_image</label>
                        @if ($errors->has('result_image'))
                            <p class="text-danger">{{ $errors->first('result_image') }}</p>
                        @endif
                        @if ($game->result_image)
                            <img style='height: 200px;object-fit: contain;' class='img-fluid  mt-2 mb-2' src='{{ asset("images/$game->result_image") }}'>
                        @endif
                        <input type="file" name="result_image"  id="result-image" class="form-control p-1" >
                    </div>

                    <div class="col-md-12 mt-2">
                        <label for="register_form">Register Form</label>
                        <div class="row" id="register_form">
                            <div class="col-md-2 ">
                                <div class="checkbox">
                                    <label><input type="radio"  name="register_form" value="1" {{$game->register_form == '1'?'checked':''}} required> Don’t show</label>
                                </div>
                            </div>
                            <div class="col-md-3 " >
                                <div class="checkbox" >
                                    <label><input type="radio"  name="register_form" value="2" {{$game->register_form == '2'?'checked':''}} required> Show on game start</label>
                                </div>
                            </div>
                            <div class="col-md-3 " >
                                <div class="checkbox" >
                                    <label><input type="radio"  name="register_form" value="3" {{$game->register_form == '3'?'checked':''}} required> Show on game result</label>
                                </div>
                            </div>
                            <div class="col-md-4 " >
                                <div class="checkbox" >
                                    <label><input type="radio"  name="register_form" value="4" {{$game->register_form == '4'?'checked':''}} required> Show on game start and result</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="form_register_name" name="form_register_name" value="1" {{$game->register_form_id==1?'checked':''}}>
                      <label class="custom-control-label" for="form_register_name">Form Register Name </label>
                    </div>
                    <div class="col-md-12  mt-3">
                        <button  class="btn btn-success ml-2 float-right">Submit</button>
                        <a href="{{ URl("Admin/Question/more/$game->id") }}" class="btn btn-outline-success ml-2 float-right">More Question</a>
                        <button type="button" class="btn btn-warning ml-2 float-right edit_question">Edit Question</button>
                        <button type="button" class="btn btn-danger ml-2 float-right delete_question">Delete Question</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
