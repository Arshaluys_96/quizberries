<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KH3STTR');</script>
<!-- End Google Tag Manager -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Trendida - Trends the fun way!</title>
@if(url()->current() == URL('/'))
    <link rel="stylesheet" href="{{ asset('css/landing/style.css') }}">
@elseif(!empty($game->css))
    <link rel="stylesheet" href="{{ asset('css/landing/'.$game->css) }}">
@elseif(!empty($quiz->css))
    <link rel="stylesheet" href="{{ asset('css/landing/'.$quiz->css) }}">
@else
    <link rel="stylesheet" href="{{ asset('css/landing/style.css') }}">
@endif
<link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Work+Sans:300,400,700" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<meta name="url" content="{{ URL('/') }}"/>
<meta name="token" content="{{ csrf_token() }}"/>
 <link rel="apple-touch-icon" sizes="57x57" href="/images/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
<link rel="manifest" href="/images/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/images/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
@if(!empty($game))
<meta property="fb:app_id"      content="514162265872345" />
<meta property="og:site_name" 	content="trendida.com" />
@if(Session::get('count_question') )
    <meta property="og:title" 		content="{{$game->questions[Session::get('count_question')-1]->title}}" />
@elseif(!empty($starts->question_number))
    <meta property="og:title" 		content="{{$game->questions[$starts->question_number-1]->title}}" />
@else
    <meta property="og:title" 		content="@lang('message.share_do_you')" />
@endif
@if(isset($starts) && $starts->registerForm)
	<meta property="og:url" 		content='{{ URL("/quiz/".$game->url."/question/".$token."/".$starts->registerForm->unique_id) }}' />
@else
	<meta property="og:url" 		content='{{ URL::current() }}' />
@endif
<meta property="og:type" 		content="website">
@if(Session::get('count_question'))
    @if(!empty($game->questions[Session::get('count_question')-1]->image))
        <meta property="og:image" 		content='{{ asset("images/".$game->questions[Session::get('count_question')-1]->image ) }}' />
    @else
        <meta property="og:image" 		content='{{ asset("images/".$game->img ) }}' />
    @endif
@elseif(!empty($starts->question_number))
    @if(!empty($game->questions[$starts->question_number-1]->image))
        <meta property="og:image" 		content='{{ asset("images/".$game->questions[$starts->question_number-1]->image) }}' />
    @else
        <meta property="og:image" 		content='{{ asset("images/".$game->img ) }}' />
    @endif
@else
    <meta property="og:image" 		content='{{ asset("images/$game->img") }}' />
@endif
<meta property="og:description" content="@lang('message.share_description')" />

<meta property="og:image:width" content="800">
<meta property="og:image:height" content="420">
@endif

<!-- Google Auto Ads -->
<script data-ad-client="ca-pub-4044693535716961" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

