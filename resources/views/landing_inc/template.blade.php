<!doctype html>
<html>
<head>
	@include('landing_inc.head')
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KH3STTR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="myJoker" class="modal fade joker_m" role="dialog" >
	<!-- Modal content-->
	<div class="modal-dialog">
		<div class="modal-content joker_modal">
			<div class="row justify-content-center">
				<i class="far fa-lightbulb fa-3x" ></i>
			</div>
			<div class="mt-4 text-center">
				<p>@lang('message.template_would')</p>
			</div>
			<div class="actions">
				<div class="row justify-content-center">
					<a href="{{ URL('/quiz/joker') }}" class="btn btn-outline-success">
						<i class="fa fa-check"></i> @lang('message.template_action_yes')
					</a>
					<button class="ml-2 btn btn-outline-danger" type="button"  data-dismiss="modal">
						<i class="fas fa-times"></i> @lang('message.template_action_no')
					</button>
				</div>
			</div>
		</div>
	</div>
</div>

@include('landing_inc.header')

@yield('contents')
@include('landing_inc.footer')
</body>
</html>
