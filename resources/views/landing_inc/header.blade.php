<nav id="navbarnew" class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a id="mainLogo" class="navbar-brand li_for_logo_img" href="{{URL('/')}}"><img alt="mainLogo" src="{{ asset('images/logo.png') }}"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item li_for_header_item">
          <a  class="nav-link a_class_for_item_menu" href="https://trendida.com"><span class='class_for_hover_effect_menu' id="morequizzes">@lang('message.header_more_quizzes')</span></a>
        </li>
      </ul>
    </div>
  </div>
</nav>