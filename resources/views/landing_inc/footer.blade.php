<div class='container'>
	<div class='row pt-5 pr-3 footerpad'>
		<div class='col-md-12 col-12 footer_part'>
			<ul class='footer_ul pt-3'>
				<li class='footer_ul_li' ><a href="{{ route('home') }}" class='a_footer_menu'> @lang('message.footer_navigation_home') </a></li>
				<li class='footer_ul_li' ><a href="{{ route('imprint') }}" class='a_footer_menu'> @lang('message.footer_navigation_imprint') </a></li>
				<li class='footer_ul_li' ><a href="{{ route('terms') }}" class='a_footer_menu'> @lang('message.footer_navigation_terms') </a></li>
				<li class='footer_ul_li' ><a href="{{ route('data-protection') }}" class='a_footer_menu'> @lang('message.footer_navigation_data_protection') </a></li>
			</ul>
		</div>
		<div class='col-md-12 col-12'>
			<div class='bottom_desc'>
				@lang('message.footer_disclaimer')
			</div>
			<div class='bottom_desc'>
				© 2019 Trendida
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('js/landing/script.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>