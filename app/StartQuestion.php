<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StartQuestion extends Model
{
    //
    protected $table = 'start_question';

    public function right()
    {
        return $this->belongsTo('App\Answers','right_answer');
    }

}
