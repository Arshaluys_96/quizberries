<?php

namespace App\Http\Controllers;

use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Nexmo\Numbers\Number;
use Session;
use App\Questions;
use App\Answers;
use App\Start;
use App\Games;
use App\StartQuestion;
use App\RegularSummaryText;
use App\RegisterForm;
use Validator;
use Redirect;
use App;
use Lang;


class GlobalController extends Controller
{
    //

    // Admin Dashborad
    public function adminIndex()
    {
        $games = Games::orderBy('id','DESC')->get();
        return view('admin.home',['games' => $games]);
    }

    // add Games page
    public function addGames()
    {
        return view('admin.games');
    }
    // Get Quiz info
    public function quizInfo($token)
    {
        Session::forget('userId');

        if($quiz = Games::where('url',$token)->first()){

            $summary_text = "";



            $count_q = Questions::where('game_id',$quiz->id)->count();
            if ( $start = Start::where('game_id',$quiz->id)->orderBy('id','desc')->first()) {
                if ($quiz->type == Games::iq_quiz){
                    $summary_text = $quiz->summary_text;

                }else{

                    $summary = RegularSummaryText::where('game_id',$quiz->id)->first();
                    $result = ($start->right_answer*100)/$count_q;

                    if ($result >= 0 && $result < 20) {
                        $summary_text = $summary->summary_text_20;
                    }else if ($result >= 20 && $result < 40) {
                        $summary_text = $summary->summary_text_40;
                    } else if($result >=40 && $result < 60) {
                        $summary_text = $summary->summary_text_60;
                    } else if($result >=60 && $result < 80) {
                        $summary_text = $summary->summary_text_80;
                    } else{
                        $summary_text = $summary->summary_text_100;
                    }

                }

            }
            $data = [
                'quiz' => $quiz,
                'summary_text' => $summary_text,
                'encode' => $this->encode($quiz->id),
            ];

            return view('landing.quiz',$data);
        }
        return Redirect::back();
    }

    // Landing Page

    public function LandingPage()
    {
        $games = Games::where(['language' => App::getLocale(),'home_screen' => 1])->orderBy('id','desc')->limit(20)->get();
        $games_top = Games::where(['language' => App::getLocale(),'home_screen' => 1])->orderBy('visit','desc')->limit(3)->get();
        $data = [
            'games' => $games,
            'games_top' => $games_top
        ];
        return view('landing.main',$data);
    }
    // create Games

    public function createGames(Request $request)
    {
        $summary_text = $request->input('summary_text');
        $title = $request->input('title');
        $url = $request->input('url');
        $sub_title = $request->input('sub_title');
        $description = $request->input('description');
        $info = $request->input('info');
        $image = $request->file('image');
        $result_image = $request->file('result_image');
        $quiz_type = $request->input('quiz_type');
        $language = $request->input('language');
        $imghidden = $request->input('imghidden');
        $css = $request->input('css');
        $stopwatch = $request->input('stopwatch');
        $timer = $request->input('timer');
        $quiz_ad_top = $request->input('quiz_ad_top');
        $quiz_ad1 = $request->input('quiz_ad1');
        $quiz_ad2 = $request->input('quiz_ad2');
        $correct = $request->input('correct');
        $home_screen = $request->input('home_screen');
        $change_audio_file = $request->input('change_audio_file');
        $correct_audio = $request->file('correct_audio');
        $incorrect_audio = $request->file('incorrect_audio');
        $box_info = $request->input('box_info');
        $register_form = $request->input('register_form');
        $register_form_id = $request->input('form_register_name');

        $rules = array(
            'title'       => 'required',
            'url'         => 'required|unique:games',
            'sub_title'   => 'required',
            'description' => 'required',
            'info'        => 'required',
            'language'    => 'required',
            'css'         => 'required',
            'image'       => 'required',
            'timer'       => 'required_if:stopwatch,1',
            'quiz_type'   => 'required|numeric|between:0,1',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator) // send back all errors to the login form
            ->withInput();
        } else {
        	$games = new Games;
        	$games->title = $title;
        	$games->sub_title = $sub_title;
            $games->description = $description;
        	$games->info = $info;
            $games->summary_text = $summary_text;
            $games->css         = $css;
            $games->img = $this->image_upload($image);
            $games->url = $url;
            $games->correct = $correct;
            $games->home_screen = $home_screen;
            $games->box_info = $box_info;
            $games->register_form = $register_form;
            $games->register_form_id = $register_form_id?1:0;

            if ($imghidden == 1){
                $games->img_hide = 1;
            }
            if (!is_null($result_image)) {

                $games->result_image = $this->image_upload($result_image);
            }
        	$games->type = $quiz_type;
            $games->language = $language;
            if ($stopwatch == 1){

                $games->stopwatch = $stopwatch;
            }

            if (!empty($quiz_ad_top)){

                $games->quiz_ad_top = $quiz_ad_top;
            }

            if (!empty($quiz_ad1)){

                $games->quiz_ad1 = $quiz_ad1;
            }

            if (!empty($quiz_ad2)){

                $games->quiz_ad2 = $quiz_ad2;
            }

            if (!empty($timer)){
                $timers = explode(':',$timer);
                if (is_numeric($timers[0]) && is_numeric($timers[1])){
                    $games->timer = $timers[0]*60+$timers[1];
                }
            }

            if ($change_audio_file == 1) {
                $games->audio_correct = $this->audio_upload($correct_audio);
                $games->audio_incorrect = $this->audio_upload($incorrect_audio);
            }

        	if ($games->save()) {

        	    if($quiz_type==1){
        	        $regular=new RegularSummaryText();
        	        $regular->game_id=$games->id;
        	        $regular->summary_text_20=$request->input('summary_text_20');
        	        $regular->summary_text_40=$request->input('summary_text_40');
        	        $regular->summary_text_60=$request->input('summary_text_60');
        	        $regular->summary_text_80=$request->input('summary_text_80');
        	        $regular->summary_text_100=$request->input('summary_text_100');
        	        $regular->save();
                }
                Session::forget('gameID');

        		return view('admin.questions',['q_number' => 1]);
        	}
        	return Redirect::back();
        }
    }


    // update Games
    public function updateGames(Request $request)
    {
        $id = $request->input('id');
        $summary_text = $request->input('summary_text');
        $title = $request->input('title');
        $sub_title = $request->input('sub_title');
        $description = $request->input('description');
        $info = $request->input('info');
        $image = $request->file('image');
        $result_image = $request->file('result_image');
        $imghidden = $request->input('imghidden');
        $css = $request->input('css');
        $url = $request->input('url');
        $stopwatch = $request->input('stopwatch');
        $timer = $request->input('timer');
        $quiz_ad_top = $request->input('quiz_ad_top');
        $quiz_ad1 = $request->input('quiz_ad1');
        $quiz_ad2 = $request->input('quiz_ad2');
        $correct = $request->input('correct');
        $home_screen = $request->input('home_screen');
        $change_audio_file = $request->input('change_audio_file');
        $correct_audio = $request->file('correct_audio');
        $incorrect_audio = $request->file('incorrect_audio');
        $box_info = $request->input('box_info');
        $register_form = $request->input('register_form');
        $register_form_id = $request->input('form_register_name');
        $rules = array(
            'title'       => 'required',
            'url'         => 'required|unique:games,url,'.$id,
            'sub_title'   => 'required',
            'description' => 'required',
            'info'        => 'required',
            'css'         => 'required',
            'timer'       => 'required_if:stopwatch,1',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator) // send back all errors to the login form
                ->withInput();
        } else {
            $games = Games::find($id);
            $games->title = $title;
            $games->sub_title = $sub_title;
            $games->description = $description;
            $games->info = $info;
            $games->summary_text = $summary_text;
            $games->css = $css;
            $games->url = $url;
            $games->quiz_ad_top = $quiz_ad_top;
            $games->quiz_ad1 = $quiz_ad1;
            $games->quiz_ad2 = $quiz_ad2;
            $games->correct = $correct;
            $games->home_screen = $home_screen;
            $games->box_info = $box_info;
            $games->register_form = $register_form;
            $games->register_form_id = $register_form_id?1:0;

            if ($stopwatch == 1){

                $games->stopwatch = $stopwatch;
            }

            if (!empty($timer)){
                $timers = explode(':',$timer);
                if (is_numeric($timers[0]) && is_numeric($timers[1])){
                    $games->timer = $timers[0]*60+$timers[1];
                }
            }
            if (!is_null($image)) {

                $games->img = $this->image_upload($image);
            }
            if (!is_null($result_image)) {

                $games->result_image = $this->image_upload($result_image);
            }
            if ($imghidden == 0){
                $games->img_hide = 0;
            }else{
                $games->img_hide = 1;
            }

            if ($change_audio_file == 1) {
                $games->audio_correct = $this->audio_upload($correct_audio);
                $games->audio_incorrect = $this->audio_upload($incorrect_audio);
            }

            if ($games->save()) {

                if($games->type == 1){

                    if (RegularSummaryText::where('game_id',$games->id)->exists()) {
                        $regular = RegularSummaryText::where('game_id',$games->id)->first();
                    } else {
                        $regular=new RegularSummaryText();
                        $regular->game_id=$games->id;

                    }

                    $regular->summary_text_20=$request->input('summary_text_20');
                    $regular->summary_text_40=$request->input('summary_text_40');
                    $regular->summary_text_60=$request->input('summary_text_60');
                    $regular->summary_text_80=$request->input('summary_text_80');
                    $regular->summary_text_100=$request->input('summary_text_100');
                    $regular->save();

                }

                return Redirect('/Admin/home');
            }
            return Redirect::back();
        }
    }


    // Create Question
    public function createQuestion(Request $request){
        $title = $request->input('title');
        $description = $request->input('interesting_fact');
        $answer1 = $request->input('answer1');
        $answer2 = $request->input('answer2');
        $answer3 = $request->input('answer3');
        $answer4 = $request->input('answer4');
        $rigth = $request->input('answer_rigth');
        $q_number = $request->input('question_number');
        $image = $request->file('image');
        $content_type = $request->input('content_type');
        $html_box = $request->input('html_box');
        $answer_number = $request->input('answer_number');
        $countdown_timer = $request->input('countdown_timer');
        $countdown = $request->input('countdown');
        $question_info = $request->input('question_info');

        $rules = array(
            'title'        		=> 'required',
            'interesting_fact'  => 'required',
            'answer1'  			=> 'required_if:answer_number,2',
            'answer2'  			=> 'required_if:answer_number,2',
            'answer3'  			=> 'required_if:answer_number,4',
            'answer4'  			=> 'required_if:answer_number,4',
            'question_number'   => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator) // send back all errors to the login form
            ->withInput();
        } else {

            $questions = new Questions;
        	$questions->title = $title;
        	$questions->description = $description;
        	$questions->question_number = $q_number;
            $questions->question_info = $question_info;
        	if (Session::get('gameID')){
                $questions->game_id = Session::get('gameID');

            } else {

                $questions->game_id = Games::orderBy('id', 'DESC')->first()->id;
            }
            $questions->answer_number = $answer_number;
            $questions->content_type = $content_type;

            if ($countdown == 1) {
                $questions->countdown_timer = $countdown_timer;
            }
            if ($content_type == 0){

                $questions->image = $this->image_upload($image);
            }elseif($content_type == 2){
                $questions->html_box = $html_box;
            }

            if (!is_null($request->input('confirm'))) {
                $questions->confirm = 1;
            }
        	if ($questions->save()) {

        	    if ($answer_number != 0) {
                    if($answer_number == 2){
                        $arr = [$answer1,$answer2];
                    }else if($answer_number == 4){

                        $arr = [$answer1,$answer2,$answer3,$answer4];
                    }
                    foreach ($arr as $key => $value) {
                        $answer = new Answers;
                        $answer->answer = $value;
                        $answer->question_id = Questions::orderBy('id', 'DESC')->first()->id;
                        if ($key+1 == $rigth) {
                            $answer->right = 1;
                        }
                        $answer->save();
                    }
                }

                return view('admin.questions',['q_number' => $q_number+1]);
        	}
        	return Redirect::back();
        }
    }

    //create mre question
    public function moreQuestion($id){
        $question = Questions::where('game_id',$id)->orderBy('id', 'DESC')->first();
        Session::put('gameID', $id);
        return view('admin.questions',['q_number' => $question->question_number+1]);
    }

    // delete question
    public function removeQuestion(Request $request){
        $id = $request->input('id');
        $game_id = $request->input('game_id');
        if ($question = Questions::where(['game_id' => $game_id,'question_number' => $id])->first()) {

            Answers::where('question_id',$question->id)->delete();

            $question->delete();

            $questions =  Questions::where(['game_id' => $game_id])->get();

            foreach ($questions as $key =>  $q) {
                $q->question_number = $key+1;
                $q->save();

            }
            return response(['success' => true]);
        }
        return response(['success' => false]);
    }

    // edit question
    public function editQuestion($id,$q_id)
    {
        $question = Questions::where(['game_id' => $id,'question_number' => $q_id])->first();
        if (!is_null($question)) {
            return view('admin.editQuestion',['question' => $question]);
        }
        return Redirect('/Admin/home');
    }

    // update question
    public function updateQuestion(Request $request)
    {
        $id = $request->input('id');
        $title = $request->input('title');
        $description = $request->input('interesting_fact');
        $answer1 = $request->input('answer1');
        $answer2 = $request->input('answer2');
        $answer3 = $request->input('answer3');
        $answer4 = $request->input('answer4');
        $rigth = $request->input('answer_rigth');
        $confirm = $request->input('confirm');
        $image = $request->file('image');
        $answer_number = $request->input('answer_number');
        $content_type = $request->input('content_type');
        $html_box = $request->input('html_box');
        $countdown_timer = $request->input('countdown_timer');
        $question_info = $request->input('question_info');

        $rules = array(
            'title'             => 'required',
            'interesting_fact'  => 'required',
            'answer1'           => 'required_if:answer_number,2',
            'answer2'           => 'required_if:answer_number,2',
            'answer3'  			=> 'required_if:answer_number,4',
            'answer4'  			=> 'required_if:answer_number,4',

        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator) // send back all errors to the login form
            ->withInput();
        } else {

            $questions = Questions::find($id);
            $questions->title = $title;
            $questions->description = $description;
            $questions->answer_number = $answer_number;
            $questions->content_type = $content_type;
            $questions->question_info = $question_info;

            if ($countdown_timer) {
                $questions->countdown_timer = $countdown_timer;
            } else {
                $questions->countdown_timer = 0;
            }

            if ($content_type == 0) {
                if (!is_null($image)) {
                    $questions->image = $this->image_upload($image);
                }
            } elseif ($content_type == 2) {
                if (!is_null($html_box)) {
                    $questions->html_box = $html_box;
                }
            }

            $questions->confirm = $confirm;

            if ($questions->save()) {


                if(count($questions->answers) == 2 && $answer_number == 4){
                    $answer = new Answers;
                    $answer->answer = $answer3;
                    $answer->question_id =$questions->id;
                    if (3 == $rigth) {
                        $answer->right = 1;
                    }
                    $answer->save();

                    $answer = new Answers;
                    $answer->answer = $answer4;
                    $answer->question_id =$questions->id;
                    if (4 == $rigth) {
                        $answer->right = 1;
                    }
                    $answer->save();
                }else if(count($questions->answers) == 4 && $answer_number == 2){
                    foreach ($questions->answers as $key => $answer) {
                        if ($key > 1){
                            $answer = Answers::find($answer->id);
                            $answer->delete();
                        }
                    };
                }

                if ($answer_number != 0) {
                    if ($answer_number == 2) {
                        $arr = [$answer1, $answer2];
                    } else {

                        $arr = [$answer1, $answer2, $answer3, $answer4];
                    }

                    $questions = Questions::find($id);
                    foreach ($questions->answers as $key => $answer) {

                        $answer = Answers::find($answer->id);
                        $answer->answer = $arr[$key];
                        if ($key + 1 == $rigth) {
                            $answer->right = 1;
                        } else {
                            $answer->right = 0;
                        }
                        $answer->save();
                    };
                }

            }
            return Redirect('/Admin/home');
        }
    }


    // duplicate ganes
    public function duplicate($id){
        if ($game = Games::find($id)){
            $newGame = $game->replicate();
            $newGame->visit = 0;
            $newGame->save();

            foreach($game->questions as $questions) {
                $newQuestion =  $questions->replicate();
                $newQuestion->game_id = $newGame->id;
                $newQuestion->save();

                foreach($questions->answers as $answers) {
                    $newanswers =  $answers->replicate();
                    $newanswers->question_id = $newQuestion->id;
                    $newanswers->save();
                }

            }


        }
        return Redirect('/Admin/home');
    }


    // update question
    public function editGames($id)
    {
        return view('admin.edit',['game' => Games::find($id)]);
    }




    // delete Games
    public function deleteGames($id)
    {
        $questions  =  Questions::where('game_id',$id)->get();
        if (!empty($questions[0])) {
            $answer_id = [];
            foreach ($questions as $question) {
                Answers::where('question_id',$question->id)->delete();
            }

            Questions::where('game_id',$id)->delete();
        }
        Games::where('id',$id)->delete();

        return Redirect::back();
    }

    // upload image
    public function image_upload($image){
        if (!empty($image)) {
            $path = public_path('images/');
            $file_name = time() . "_" . $image->getClientOriginalName();
            $image->move($path, $file_name);
            return $file_name;
        }
    }

    // upload image
    public function audio_upload($audio){
        if (!empty($audio)) {
            $path = public_path('audio/');
            $file_name = time() . "_" . $audio->getClientOriginalName();
            $audio->move($path, $file_name);
            return $file_name;
        }
    }


    public function register(Request $request,$url,$token){
        if ($request->input('phone')) {
            do{
                $unique_id = Str::random(5);
            }while(RegisterForm::where('unique_id', $unique_id)->exists());

            $register = new RegisterForm();
            $register->unique_id = $unique_id;
            $register->name = $request->input('name');
            $register->phone = $request->input('phone');
            if (Session::has('registerUser')) {
                $register->ref = Session::get('registerUser');
                Session::forget('registerUser');
            }
            $register->save();

            Session::put('RegisterForm', $register->id);
        }
        return Redirect(route('quiz.question',['url' => $url,'token' => $token]));
    }

    public function quizRegister(Request $request){
        if ($request->input('phone')) {
            do{
                $unique_id = Str::random(5);
            }while(RegisterForm::where('unique_id', $unique_id)->exists());

            $register = new RegisterForm();
            $register->unique_id = $unique_id;
            $register->name = $request->input('name');
            $register->phone = $request->input('phone');
            $register->save();

            $start_id = base64_decode($request->input('start_id'));
            $start = Start::find($start_id);
            $start->register_form_id = $register->id;
            $start->save();

            Session::put('RegisterForm', $register->id);
        }
        return Redirect::back();
    }

    public function registerUser($token,$user){
        if ($token && $user) {
            Session::put('registerUser', $user);
        }
        return Redirect(URL("quiz/".$token."/info"));
    }


    // quiz start
    public function quiz($url,$token,$user = null){
        if(!is_null($user)) {
            Session::put('registerUser', $user);
        }
        $token = $this->decode($token);
        if(!($game = Games::where('url',$url)->first())){
            return Redirect::back();
        }

        if (empty(Session::get('userId'))){
            $start = new Start();
            $start->game_id = $game->id;
            if ($game->stopwatch == 1){
                $start->timer = $game->timer;
            }
            if (Session::has('RegisterForm')) {
                $start->register_form_id = Session::get('RegisterForm');
            }
            $start->save();

            $Games = Games::find($game->id);
            $Games->visit = $game->visit+1;
            $Games->save();


            $start = Start::orderBy('id', 'DESC')->first();

            Session::put('userId', $start->id);
            Session::forget('count_question');
            Session::forget('exist_question');
            Session::forget('RegisterForm');
            if($token > 1){
                Session::put('count_question', $token);
            }
            Session::put('counts',1);
        }else if(Start::find(Session::get('userId'))->game_id != $game->id){
            Session::forget('userId');
            $start = new Start();
            $start->game_id = $game->id;
            if ($game->stopwatch == 1){
                $start->timer = $game->timer;
            }
            if (Session::has('RegisterForm')) {
                $start->register_form_id = Session::get('RegisterForm');
            }
            
            $start->save();

            $Games = Games::find($game->id);
            $Games->visit = $game->visit+1;
            $Games->save();

            $start = Start::orderBy('id', 'DESC')->first();

            Session::put('userId', $start->id);
            Session::forget('count_question');
            Session::forget('exist_question');
            Session::forget('RegisterForm');
            if($token > 1){
                Session::put('count_question', $token);
            }
            Session::put('counts',1);
        }
        else{

            if(Session::get('current') != url()->current()){
                if (!empty(Session::get('counts'))){
                    if(URL()->previous() != URL()->current()){
                        Session::put('counts',Session::get('counts')+1);
                    }
                }else{
                    Session::put('counts',1);
                }

                if(!empty(Session::get('count_question'))){
                    Session::put('exist_question',Session::get('count_question'));
                    Session::forget('count_question');
                }
            }
            $start = Start::find(Session::get('userId'));
            if($start->timer < 1 && $game->stopwatch == 1){
                return Redirect('/');
            }
        }
//
//        if ( $start->question_number == StartQuestion::where('start_id',$start->id)->orderBy('id', 'desc')->count()) {
//
//        }


        if ($game->questions[$start->question_number-1]->answer_number != 0) {
            $right = Answers::where(['right' => 1,'question_id' => $game->questions[$start->question_number-1]->id])->first();
            $startQuestion = StartQuestion::where('start_id',$start->id)->orderBy('id', 'desc')->count();
            if ($start->question_number-1 == $startQuestion) {
                $startQuestion = new StartQuestion();
                $startQuestion->start_id = $start->id;
                $startQuestion->game_id = $game->id;
                $startQuestion->right_answer = $right->id;
                $startQuestion->second = $game->questions[$start->question_number-1]->countdown_timer;
                $startQuestion->save();
            }

        }

        Session::put('current',url()->current());

        return view('landing.start',['game' => $game,'starts' => $start,'token' => $token]);
    }

    // check timer
    public function next(){
        if ($start = Start::find(Session::get('userId'))){
            if ($start->start_questions[$start->start_questions->count()-1]->second == 0) {
                $game = Games::find($start->game_id);

                $q_number = $start->question_number;
                $count_q = Questions::where('game_id',$start->game_id)->count();
                if ($count_q == $q_number) {
                    $start->save();
                    return Redirect('/quiz/calculating');
                }
                if(Session::get('exist_question') == $q_number+1){

                    $start->question_number = $q_number+2;
                }else{
                    $start->question_number = $q_number+1;
                }
                $start->save();
                return Redirect(route('quiz.question',['url' => $game->url,'token' => $this->encode($start->game_id)]));
            }

            return Redirect::back();
        }
        return Redirect('/');
    }

    // check timer
    public function timer(){
        if ($start = Start::find(Session::get('userId'))){
            if ($start->timer > 0){
                $timer = $start->timer -1;
                $start->timer = $timer;
                $start->save();
                return response(['success' => true,'time' => $timer]);
            }
        }
        return response(['success' => false]);
    }

    // check second
    public function second(){
        if ($startQuestion= StartQuestion::where('start_id',Session::get('userId'))->orderBy('id', 'desc')->first()) {
            if ($startQuestion->second > 0) {
                $timer = $startQuestion->second - 1;
                $startQuestion->second = $timer;
                $startQuestion->save();
                return response(['success' => true, 'second' => $timer]);
            } else {
                $start = Start::find(Session::get('userId'));
                $game = Games::find($start->game_id);
                $q_number = $start->question_number;
                $question = Questions::where(['game_id' => $start->game_id,'question_number' => $q_number])->first();
                $last_question = false;
                if ($game->questions->count() == $q_number) {
                    $last_question = true;
                }
                foreach ($question->answers as $key => $answer) {
                    if ($answer->right == 1) {
                        return response(['success' => false,'right' => $key,'second' => 0,'last_question' => $last_question]);
                    }
                }
            }
        }
        return response(['success' => false]);
    }



    // quiz test
    public function quizTest(Request $request)
    {
        $quiz = base64_decode($request->input('quiz'));
        $start = Start::find(Session::get('userId'));
        $startQuestion = StartQuestion::where('start_id',$start->id)->orderBy('id','desc')->first();
        if ($startQuestion->check != 1 && (is_null($request->input('quiz')) || !is_numeric($quiz) || ($quiz <  0 || $quiz > 3)) ) {
            return Redirect::back();
        }else if(empty(Session::get('userId'))){
            return Redirect('/');
        }

        if (is_null($request->input('quiz')) && $startQuestion->check == 1) {
            $quiz = Session::get('quiz_refresh');
        }

        $game = Games::find($start->game_id);
        if(!empty(Session::get('count_question'))) {
            $q_number =  Session::get('count_question');
            $right = 0;
            $question = Questions::where(['game_id' => $start->game_id,'question_number' => $q_number])->first();
            if ($question->confirm == 1) {
                Session::forget('quiz');
                Session::put('quiz', $quiz);
                return Redirect(route('confirm',['url' => $game->url,'token' => $this->encode($start->game_id)]));
            }

            if ($question->answer_number != 0) {
                if ($question->answers[$quiz]->right == 1) {

                    $start->right_answer = $right+1;
                }

                $startQuestion->your_answer = $question->answers[$quiz]->id;
                $startQuestion->save();
            }


            $start->question_number = 1;
            $start->save();
        }else{
            $q_number = $start->question_number;
            $right = $start->right_answer;
            $question = Questions::where(['game_id' => $start->game_id,'question_number' => $q_number])->first();
            if ($question->confirm == 1) {
                Session::forget('quiz');
                Session::put('quiz', $quiz);

                return Redirect(route('confirm',['url' => $game->url,'token' => $this->encode($start->game_id)]));
            }
            if ($question->answer_number != 0) {
                if ($question->answers[$quiz]->right == 1) {

                    $start->right_answer = $right + 1;
                }

                $startQuestion->your_answer = $question->answers[$quiz]->id;
                $startQuestion->save();
            }

            $q_number = $start->question_number;

            $count_q = Questions::where('game_id',$start->game_id)->count();
            if ($count_q == $q_number) {
                $start->save();
                return Redirect('/quiz/calculating');
            }
            if(Session::get('exist_question') == $q_number+1){

                $start->question_number = $q_number+2;
            }else{
                $start->question_number = $q_number+1;
            }
            $start->save();
        }

        Session::forget('quiz_refresh');
        return Redirect(route('quiz.question',['url' => $game->url,'token' => $this->encode($start->game_id)]));

    }

    public function Confirm($url,$token){
        $token = $this->decode($token);
        $next = false;
        if(!empty(Session::get('userId'))){
            if(!($game = Games::where('url',$url)->first())) {
                return Redirect::back();
            }
            $start = Start::find(Session::get('userId'));
            if ($game->id != $start->game_id) {
                $next = false;
            } else {
                $next = true;
            }
        }else{
            if(Games::where('url',$url)->first()){
                return Redirect(URL("quiz/".$url."/info"));
            }
            return Redirect('/');
        }
        return view('landing.confirm',['game' => $game,'count' => $token-1,'next' => $next]);
    }

    public function encode($id){
        $game = Games::find($id);
        if (!empty(Session::get('userId'))){
            $start = Start::find(Session::get('userId'));
            $q_number = $start->question_number;
        }else{
            $q_number = 1;
        }
        $question = Questions::where(['game_id' => $game->id,'question_number' => $q_number])->first();
        if(is_null($question)){
            return false;
        }
        return base64_encode(base64_encode($q_number));
    }

    public function decode($encode){
        return base64_decode(base64_decode($encode));
    }

    // confirm
    public function quizConfirm(){
        if(empty(Session::get('userId'))){
            return Redirect('/');
        }else if(is_null(Session::get('quiz'))){
            $start = Start::find(Session::get('userId') );
            $game = Games::find($start->game_id);
            return Redirect(route('quiz.question',['url' => $game->url,'token' => $this->encode($game->id)]));
        }
        $quiz = Session::get('quiz');
        $start = Start::find(Session::get('userId'));

        $startQuestion = StartQuestion::where('start_id',$start->id)->orderBy('id','desc')->first();

        if(!empty(Session::get('count_question'))) {
            $q_number =  Session::get('count_question');
            $right = 0;
            $question = Questions::where(['game_id' => $start->game_id,'question_number' => $q_number])->first();
            if ($question->answer_number != 0) {
                $startQuestion->your_answer = $question->answers[$quiz]->id;
                $startQuestion->save();

                if ($question->answers[$quiz]->right == 1) {
                    $start->right_answer = $right + 1;
                }
            }
            $start->question_number = 1;

        }else{
            $q_number = $start->question_number;
            $right = $start->right_answer;
            $question = Questions::where(['game_id' => $start->game_id,'question_number' => $q_number])->first();

            if ($question->answer_number != 0) {
                $startQuestion->your_answer = $question->answers[$quiz]->id;
                $startQuestion->save();
                if (!is_null($question) && $question->answers[$quiz]->right == 1) {

                    $start->right_answer = $right + 1;
                }
            }

            $count_q = Questions::where('game_id',$start->game_id)->count();


            if(Session::get('exist_question') == $q_number+1){

                $start->question_number = $q_number+2;
                $q_number += 2;
            }else{
                $start->question_number = $q_number+1;
            }

            if ($count_q <= $q_number) {
                $start->save();
                return Redirect('/quiz/calculating');
            }
        }
        $start->save();

        $game = Games::find($start->game_id);
        return Redirect(route('quiz.question',['url' => $game->url,'token' => $this->encode($game->id)]));
    }

    // quiz check
    public function quizCheck(Request $request){
        if (!empty(Session::get('userId'))) {
            $quiz = (int)base64_decode($request->input('quiz'));
            Session::put('quiz_refresh',$quiz);
            $start = Start::find(Session::get('userId'));
            $game = Games::find($start->game_id);

            if ($game->correct == 0) {
                return response(['success' => true,'correct' => 0]);
            }
            $startQuestion = StartQuestion::where('start_id',$start->id)->orderBy('id','desc')->first();
            if ($startQuestion->check == 0) {
                $startQuestion->check = 1;
                $startQuestion->save();

                $q_number = $start->question_number;
                $question = Questions::where(['game_id' => $start->game_id,'question_number' => $q_number])->first();

                if ($game->questions->count() == $q_number) {
                    $last_question = Lang::get('message.finish_question');
                } else {
                    $last_question = Lang::get('message.next_question');
                }

                if ($question->answers[$quiz]->right == 1) {
                    return response(['success' => true,'last_question' => $last_question,'right' => $quiz,'your' => $quiz,'correct' => 1,'incorrect_audio' => $game->audio_incorrect,'correct_audio' => $game->audio_correct]);
                } else {
                    foreach ($question->answers as $key => $answer) {
                        if ($answer->right == 1) {
                            return response(['success' => true,'last_question' => $last_question,'right' => $key,'your' => $quiz,'correct' => 1,'incorrect_audio' => $game->audio_incorrect,'correct_audio' => $game->audio_correct]);
                        }
                    }
                }
            }
        }
        return response(['success' => false]);
    }


    // quiz check refresh
    public function quizCheckRefresh(Request $request){
        if (!empty(Session::get('userId'))) {
            $start = Start::find(Session::get('userId'));
            $game = Games::find($start->game_id);

            $startQuestion = StartQuestion::where('start_id',$start->id)->orderBy('id','desc')->first();
            if ($startQuestion->check == 1) {

                $q_number = $start->question_number;
                if ($game->questions->count() == $q_number) {
                    $last_question = Lang::get('message.finish_question');
                } else {
                    $last_question = Lang::get('message.next_question');
                }

                $answered_txt = Lang::get('message.answered_txt');

                return response(['success' => true,'last_question' => $last_question,'answered_txt' => $answered_txt]);

            }
        }
        return response(['success' => false]);
    }

    // loader
    public function quizCalculating(){
        if (empty(Session::get('userId'))) {
            return Redirect('/');
        }
        $start = Start::find(Session::get('userId'));
        $game = Games::find($start->game_id);
        $token = base64_encode($start->id.' '.$game->title);
        $question = Questions::where('game_id' ,$start->game_id)->orderBy('id', 'desc')->first();
        return view('landing.loader',['game' => $game,'token' => $token,'question' => $question]);
    }

    // result
    public function quizResult($url,$token,$user = null){
        if(!is_null($user)) {
            Session::put('registerUser', $user);
        }
        if (\request('fbclid')) {
            return Redirect(URL("quiz/".$url."/info"));
        }

        $token1= explode(" ",base64_decode($token));
        if ($start = Start::find($token1[0])){
            $count_q = Questions::where('game_id',$start->game_id)->count();
            if($game = Games::where('url',$url)->first()){
                if ($game->type == Games::iq_quiz){
                    $result = 30+(170/$count_q)*$start->right_answer;
                    $summary_text = $game->summary_text;
                }else{
                    $result = ($start->right_answer*100)/$count_q;

                    $summary = RegularSummaryText::where('game_id',$game->id)->first();

                    if ($result >= 0 && $result < 20) {
                        $summary_text = $summary->summary_text_20;
                    }else if ($result >= 20 && $result < 40) {
                        $summary_text = $summary->summary_text_40;
                    } else if($result >=40 && $result < 60) {
                        $summary_text = $summary->summary_text_60;
                    } else if($result >=60 && $result < 80) {
                        $summary_text = $summary->summary_text_80;
                    } else{
                        $summary_text = $summary->summary_text_100;
                    }

                }
                Session::forget('userId');

                $correct_answers = [];

                $correct_answer = StartQuestion::where('start_id',$start->id)->get();
                foreach ($correct_answer as $answer) {
                    if ($answer->your_answer == $answer->right_answer) {
                        $correct_answers[] = ['question_number' => $answer->right->question->question_number ,'question' => $answer->right->question->title,'answer' => $answer->right->answer];
                    }
                }
                return view('landing.result',['start' => $start,'game' => $game,'result' => $result,'summary_text' => $summary_text,'correct_answers' => $correct_answers,'question' => $count_q,'token' => $token]);
            }
        }

        return Redirect('/');
    }

    //share
    public function result($token){
        if (!is_null(Input::get('fbclid'))) {
            $token = explode(" ",base64_decode($token));
            if($token[0] == 'results'){
                $game = Games::find($token[1]);
                return view('landing.share',['game' => $game]);
            }else if ($start = Start::find($token[0])){
                $count_q = Questions::where('game_id',$start->game_id)->count();
                $game = Games::find($start->game_id);
                if ($game->type == Games::iq_quiz){
                    $result = 30+(170/$count_q)*$start->right_answer;
                }else{
                    $result = ($start->right_answer*100)/$count_q;
                }
                return view('landing.share',['start' => $start,'game' => $game,'result' => $result]);
            }
        }
        return Redirect('/');
    }

    //setName
    public function setName(Request $request)
    {
        $token =  explode(" ",base64_decode($request->input('param')));
        if ($start = Start::find($token[0])) {
            if (!empty($request->input('name'))){
                $start->name = $request->input('name');
                $start->save();
            }
        }
    }

    public function quizJoker(){
        $start = Start::find(Session::get('userId'));
        if ($start->joker == 0) {
            if(Session::get('count_question')){
                $question = Questions::where(['game_id' => $start->game_id,'question_number' => Session::get('count_question')])->first();
            }else{

                $question = Questions::where(['game_id' => $start->game_id,'question_number' => $start->question_number])->first();
            }

            $arr = [];
            $i = 0;
            $j = 0;
            while (count($arr) < 1) {
                if ($question->answers[$i]->right == 0) {
                    $arr[] = ['answer' => $question->answers[$i]->answer,'id' => $i];
                }
                $i++;
            }
            while (count($arr) < 2) {
                if ($question->answers[$j]->right == 1) {
                    $arr[] = ['answer' => $question->answers[$j]->answer,'id' => $j];
                }
                $j++;
            }
            Session::put('Joker', $arr);
            $start->joker = 1;
            $start->save();
        }
        return Redirect::back();
    }
}
