<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegularSummaryText extends Model
{
    //
    protected $table = 'regular_summary_text';
}
