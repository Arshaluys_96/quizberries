<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Games extends Model
{
    //
    const iq_quiz = 0;
    const regular_quiz = 1;

  	public function questions()
  	{
    	return $this->hasMany('App\Questions','game_id');
  	}

    public function regularSummaryText()
    {
        return $this->belongsTo('App\RegularSummaryText','id','game_id');
    }
}
