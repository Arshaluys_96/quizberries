<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Start extends Model
{
    //
        protected $table = 'start';

    public function start_questions()
    {
        return $this->hasMany('App\StartQuestion','start_id');
    }

        public function registerForm()
    {
        return $this->belongsTo('App\RegisterForm','register_form_id','id');
    }
}


